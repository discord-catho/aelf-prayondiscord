from setuptools import setup, find_packages

setup(
    name='mon_script',
    version='1.0',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'mon_script = main:main'
        ]
    },
)


# TODO :
# - creer repertoire "bot"
# - tout deplacer dans "bot", et creer un "__init__"
# - creer un tools et y mettre sql_script
