# AELF - Prier sur Discord

AELF - Prier sur Discord est une application pour prier les offices des heures sur le logiciel Discord en communauté.

## Description

Cette application permet de retrouver l'intégralité de la liturgie des heures (lectures, laudes, tierces, sextes, nones, vêpres et complies) sur Discord sans avoir à quitter le logiciel.

#### Ce que cette application n'est pas :

- un bréviaire : les textes sont présents, ainsi qu'une doxolgie et la répétition de l'antienne, cependant, le confiteor ne l'est pas. Le contenu permet tout de même à ceux n'ayant pas de bréviaire (n'hésitez pas à en acheter un !) ou n'en ayant pas sur eux de suivre l'office.

- une Bible : si l'AELF propose une Bible sur son site (https://www.aelf.org/bible), cette application ne peut pas proposer ce service étant donné que le contenu n'est pas disponible via l'API. N'hésitez donc pas à aller en librairie acheter une Bible, ou à consulter quand vous êtes en déplacement la traduction liturgique sur le site ou l'une des applications mobiles (iOS et Android).

## Relevés de bug

- [ ] à faire

## Données personnelles

- [ ] à faire

Voir le fichier "privacy" pour les détails sur les données personnelles.

L'équipe de développement ne reçoit aucune donnée personnelle, relevé de bug compris.
Cependant, les propriétaires d'instances peuvent avoir accès à certaines données (serveur, utilisateur ayant installé, utilisateurs avec permissions spéciales, ...) via la base de données de l'application.

## À propos

### Application

Le développement de cette application est un projet bénévole, qui n'a donc aucune de visée lucrative. Tout contributeur est le bienvenue.

### AELF

Cette application utilise les textes proposés par l'*Association épiscopale liturgique pour les pays francophones* (http://www.aelf.org) avec leur gracieuse authorisation. Les textes sont récupérés via l'API. L'AELF n'est pas responsable de cette application.

### Discord

Cette application est faite pour fonctionner sur le logiciel Discord (https://discord.com). Si vous désirez un portage sur un autre logiciel, n'hésitez pas à vous inspirer du code source ici, qui est entièrement libre. Un effort est mis sur les commentaires et la documentation.

## Installation
- Utiliser requirements et lancer avec Python
- Docker : a priori, un docker-compose suffit. Les variables d'environnement sont dans docker/python.env

```bash
$ mkdir -p aelf_prayondiscord_myinstance && cd aelf_prayondiscord_myinstance
$ wget https://framagit.org/discord-catho/aelf-prayondiscord/-/raw/main/docker/python.env
$ wget https://framagit.org/discord-catho/aelf-prayondiscord/-/raw/main/docker/docker-compose.yml
$ wget https://framagit.org/discord-catho/aelf-prayondiscord/-/raw/main/docker/Dockerfile
```

Installation
* Configurer python.env
* Construire et lancer le container `$ docker compose up -d`

Mise à jour
* Reconstruire et lancer le container `$ docker compose build --no-cache && docker compose up -d`

Générer des clés pour activer des options de maintenance et de développement avancées
* Clé privée
openssl genpkey -algorithm RSA -out private_key.pem -aes256 -pass pass:none
* Clé public (lors du mot de passe, entrer "none")
openssl rsa -in private_key.pem -outform PEM -pubout -out public_key.pem

Environnement:
BOTAELF_DEVELOPMENTMODE_PUBLIC_RSAKEY_PATH ou BOTAELF_DEVELOPMENTMODE_PUBLIC_RSAKEY
BOTAELF_DEVELOPMENTMODE_PUBLIC_RSAKEY_PASSWD="none"


## Utilisation

Demarrer avec le script ou via le docker ! Cela devrait fonctionner sinon (en placant toutes les donnees d'execution dans le dossier src), mais non testé ! Risque que le .env soit mal chargé également !

Definir les autorisations : par defaut, seuls les administrateurs ont le droit de l'utiliser.

## Developpeurs

Quelques fichiers :

* ./lang : traductions existantes des messages et des commandes
* ./src/__constants.py : les constantes utilisé dans le code pour éviter d'en définir en dur

* ./src/core : le coeur du bot, sans les commandes de base
* ./src/interactions : les commandes, taches, etc. : si "core_X", fait partie du fonctionnement du bot, sinon, est propre à ce bot
* ./src/lib : des modules assez généralistes, importable via ./aelf_prayondiscord_as_module
* ./src/utils : des modules spécifiques à ce bot, importable via ./aelf_prayondiscord_as_module

## À faire

- [ ] RELEASE_NOTES
- [X] Application "de base"
- [X] BdD permissions

- tools/sql_scripts, docker ? eventuellement un autre nom, car **t**asks
- events : a categoriser ?
- lib ou utils ?

- [X] Renommer utils/base_utils en lib : Cela a du sens, surtout si ce sont les outils de base qui sont utilisés à plusieurs endroits dans votre code. "Lib" est une abréviation couramment utilisée pour "library" (bibliothèque), ce qui correspond bien à la fonction de ces modules.
 - [ ] todo : ajouter au readme un mot : pour main, core

- [X] Renommer utils/commands_utils en utils : Si ces utilitaires sont spécifiques aux commandes dans cogs/cogs_office, alors les regrouper sous utils a du sens. Cela signifie que ces utilitaires sont spécifiques à cette partie du code.
 - [ ] todo : ajouter au readme un mot : pour cogs, tasks, events ; mais pour les modules, chacun se gérera indépendamment

- [ ] Déplacer sql_scripts sous tools : Cela a du sens car "sql_scripts" sont des outils pour la gestion de la base de données, ce qui correspond bien à l'objectif de "tools".

- [ ] Deroulements
 - [X] via API
 - [ ] via BdD (optionnel) => nom de table ? => utiliser les JSON pour (re)initialiser le remplissage de la table ?
 - [X] via fichiers JSON
- [ ] Traduction implémenter utilisation YML
- [X] Lien pour s'abonner aux lectures quotidiennes (dernier embed)
- [ ] Commande "informations" : cf dev_get_informations
- [X] Changer région dans BdD
 - [X] Creer la commande pour changer ça
- [X] Utiliser clee public pour se connecter, non ?
- [ ] Mode dev (installer iosync) : $ iosync -r src/ "python3 my_bot.py" ?
 - [ ] implémentation dans Python : dev_autorestart_bot
- [X] Commande permissions : gérer les perm
 - [ ] à terminer : il faudrait créer une liste des commandes/groupes de commandes qqpart
- [ ] Creer une classe fille à lib.dbguilds : utils.dbaelfguild : cf dev_aelfdb_children

- [ ] Gestion des plugins

### Future version
 - [ ] Sauvegarde "cache" des offices (format HTML ? format Discord ?) pendant 7 jours : demander à l'AELF (gestion via commandes)