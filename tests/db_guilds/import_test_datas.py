import json
import sys
import os
sys.path.append(os.path.abspath('./../../utils'))
from db_guilds import DBGuilds

class testDB(DBGuilds):
    def __init__(self, **options):
        options["path"] = "./test.db"
        super().__init__(**options)
    #

    def import_data_from_json(self, json_file):
        """
        Importe des données depuis un fichier JSON dans la base de données.

        :param str json_file: Chemin vers le fichier JSON.
        """
        with open(json_file, 'r') as file:
            data = json.load(file)
            for entry in data:
                # Vérification si la guilde existe
                query_guild = "SELECT 1 FROM guilds WHERE id_guild = ?"
                guild_exists = self.request_db(query_guild, (entry['guild_id'],))#.fetchone()

                if not guild_exists:
                    query = """
                    INSERT INTO guilds (id_guild, guild_name, installer_id)
                    VALUES (?, ?, ?)
                    """
                    self.request_db(query, (entry['guild_id'],"test",0))
                #
                # La guilde existe, on peut ajouter l'entrée dans permissions_commands
                query = """
                INSERT INTO permissions_commands (guild_id, command_name, allowed, channel_id, user_id, group_id)
                VALUES (?, ?, ?, ?, ?, ?)
                """
                params = (
                    entry['guild_id'],
                    entry['command_name'],
                    entry['allowed'],
                    entry['channel_id'],
                    entry['user_id'],
                    entry['group_id']
                )
                self.request_db(query, params)
                self.commit()
            #
        #
    #
