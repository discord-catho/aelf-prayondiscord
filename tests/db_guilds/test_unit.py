from import_test_datas import testDB
import unittest
from unittest import mock
import json, os, shutil, sqlite3

class TestDBImportFunctions(unittest.TestCase):

    def setUp(self):
        source_path = 'test.db.bkp'
        self.path = 'test.db'

        # Vérifier si la cible existe et la supprimer si nécessaire
        if os.path.exists(self.path):
            os.remove(self.path)
        #
        # Copier le fichier source vers la cible
        shutil.copy(source_path, self.path)

        # Initialisation de la base de données (à faire avant chaque test)
        self.db = testDB()
        # self.db.create_tables()

    def tearDown(self):
        # Nettoyage de la base de données (à faire après chaque test)
        self.db.close()

    def test_import_data_from_json(self):
        # Préparation des données pour le test
        json_data = [
            {
                "guild_id": 1,
                "command_name": "test_command",
                "allowed": True,
                "channel_id": 1,
                "user_id": 1,
                "group_id": None
            }
        ]
        with mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(json_data))):
            self.db.import_data_from_json('test_data.json')
        
        # Vérification que les données ont été correctement importées
        connection = sqlite3.connect(self.path)
        cursor = connection.cursor()

        query = "SELECT COUNT(*) FROM permissions_commands"
        #query = "SELECT * FROM permissions_commands"
        # Exécuter la requête
        cursor.execute(query)

        # Récupérer le résultat
        result = cursor.fetchone()
        self.assertEqual(result[0], 1)

    def test_import_data_from_json_extra_keys(self):
        # Préparation des données pour le test
        json_data = [
            {
                "descr":"coucou !!!!",
                "guild_id": 1,
                "command_name": "test_command",
                "allowed": True,
                "channel_id": 1,
                "user_id": 1,
                "group_id": None
            }
        ]
        with mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(json_data))):
            self.db.import_data_from_json('test_data.json')
        
        # Vérification que les données ont été correctement importées
        connection = sqlite3.connect(self.path)
        cursor = connection.cursor()

        query = "SELECT COUNT(*) FROM permissions_commands"
        #query = "SELECT * FROM permissions_commands"
        # Exécuter la requête
        cursor.execute(query)

        # Récupérer le résultat
        result = cursor.fetchone()
        self.assertEqual(result[0], 1)



    def test_import_data_from_file(self):
        # Préparation des données pour le test

        self.db.import_data_from_json('./datas.json')
        
        # Vérification que les données ont été correctement importées
        connection = sqlite3.connect(self.path)
        cursor = connection.cursor()

        query = "SELECT COUNT(*) FROM permissions_commands"
        #query = "SELECT * FROM permissions_commands"
        # Exécuter la requête
        cursor.execute(query)

        # Récupérer le résultat
        result = cursor.fetchone()
        self.assertEqual(result[0], 11)
