from import_test_datas import testDB
import unittest
from unittest import mock
import json, os, shutil, sqlite3

class TestDBCheckPermissions(unittest.TestCase):

    def setUp(self):
        source_path = 'test.db.bkp'
        self.path = 'test.db'

        # Vérifier si la cible existe et la supprimer si nécessaire
        if os.path.exists(self.path):
            os.remove(self.path)
        #
        # Copier le fichier source vers la cible
        shutil.copy(source_path, self.path)

        # Initialisation de la base de données (à faire avant chaque test)
        self.db = testDB()
        self.db.import_data_from_json('./datas.json')
        # self.db.create_tables()
        self.opt = {
            'guild_id':None,
            'id_user':None,
            'id_group':None,
            'command_name':None,
            'channel_id':None
        }

    def tearDown(self):
        # Nettoyage de la base de données (à faire après chaque test)
        self.db.close()

    def test_permissions_1(self):

        self.opt['guild_id'] = 1
        result = self.db.check_permissions(**self.opt['guild_id'])
        self.assertTrue(result)
        
