## Traductions

Chaque langue a deux (pour le moment) fichiers:

- fr.json : contient des contenus fixés
- fr_cogs.json : contient les commandes et leur description (utilisé par les fonctions du dossier : ./cogs)

Ici :
- fr = francais (de France métropolitaine)