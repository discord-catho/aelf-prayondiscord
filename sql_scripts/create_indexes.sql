-- Création de l'index sur les colonnes "channel, user, group" de la table "aelf_authorized_channels"
CREATE INDEX IF NOT EXISTS idx_authorization_commands ON permissions_commands (guild_id, allowed, command_name, channel_id, user_id, role_id);

