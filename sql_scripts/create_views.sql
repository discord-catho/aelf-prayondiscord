-- create view
CREATE VIEW IF NOT EXISTS permissions_view AS
SELECT
  g.id_guild AS guild_id,
  g.guild_name,
  g.authorized_guild,
  g.developper_mode,
  g.installer_id,
  ago.use_hunfolding,
  ago.zone,
  pco.id_command AS command_id,
  pco.command_name,
  pco.allowed AS allowed,
  pco.channel_id,
  pco.user_id,
  pco.role_id
FROM guilds g
LEFT JOIN permissions_commands pco ON g.id_guild = pco.guild_id
LEFT JOIN aelf_guilds ago ON g.id_guild = ago.id_guild;
