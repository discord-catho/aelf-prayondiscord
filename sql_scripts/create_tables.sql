-- Création de la table "aelf_guilds" pour stocker les serveurs
-- installer_id: id de l'utilisateur qui a installe
-- authorized_guild: guild validee par l'administrateur de l'instance (bloque les autres)

CREATE TABLE IF NOT EXISTS guilds (
  id_guild INTEGER PRIMARY KEY DEFAULT 0,
  guild_name TEXT NOT NULL,
  installer_id INTEGER NOT NULL,
  developper_mode BOOLEAN NOT NULL DEFAULT 0,
  authorized_guild BOOLEAN NOT NULL DEFAULT 1
);

-- comportement des commandes
-- Si user et group est a 0, authoriser tout le monde (si allowed)
-- allowed : si true, alors on autorise les utilisateurs et groupes mentionnes (et on interdit par defaut), si false, inverse
-- channel_id null : comportement pour tout le serveur
CREATE TABLE IF NOT EXISTS permissions_commands (
  id_command INTEGER PRIMARY KEY AUTOINCREMENT,
  guild_id INTEGER NOT NULL,
  command_name TEXT,
  allowed BOOLEAN NOT NULL DEFAULT 1,
  channel_id INTEGER,
  user_id INTEGER,
  role_id INTEGER,
  FOREIGN KEY (guild_id) REFERENCES guilds (id_guild)
);


CREATE TABLE IF NOT EXISTS aelf_guilds (
  id_guild INTEGER PRIMARY KEY AUTOINCREMENT,
  use_hunfolding TEXT NOT NULL DEFAULT './src/assets',
  zone TEXT NOT NULL DEFAULT 'romain',
  FOREIGN KEY (id_guild) REFERENCES guilds (id_guild)
);

CREATE TABLE IF NOT EXISTS aelf_assets_elements (
  id_asset INTEGER PRIMARY KEY AUTOINCREMENT,
  guild_id INTEGER NOT NULL,
  key_name TEXT,
  filename TEXT,
  FOREIGN KEY (guild_id) REFERENCES guilds (id_guild)

);
