-- Contenu du fichier "create_db.sql"
.read create_tables.sql

-- Contenu du fichier "create_trigger.sql"
-- .read create_triggers.sql

-- Contenu du fichier "create_views.sql"
.read create_views.sql

-- Contenu du fichier "create_indexes.sql"
.read create_indexes.sql




-- ----------------------------------------
-- (apt install sqlite3) && sqlite3 -batch ../datas/bot_aelf.db < create_database.sql

-- Ou Python :
-- import sqlite3
--
-- # Établir la connexion à la base de données SQLite
-- connexion = sqlite3.connect('nom_de_la_base_de_donnees.db')
--
-- # Créer un objet cursor
-- cursor = connexion.cursor()
--
-- # Lire le contenu du fichier SQL et exécuter le script
-- with open('database.sql', 'r') as fichier_sql:
--     script_sql = fichier_sql.read()
--     cursor.executescript(script_sql)
-- #
-- # Fermer le curseur et la connexion
-- cursor.close()
-- connexion.close()
