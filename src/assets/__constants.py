# Attention
# modifier les valeurs dans ce fichier peut modifier le comportement du bot
# si la base de donnée n'est pas mise à jour en même temps !


# ================== CORE ==================


AUTHORIZE_COMMAND_DISABLED=0
AUTHORIZE_COMMAND_ENABLED=1
AUTHORIZE_COMMAND_DEFAULT=AUTHORIZE_COMMAND_DISABLED

AUTHORIZE_GUILD_DISABLED=0
AUTHORIZE_GUILD_ENABLED=1
AUTHORIZE_GUILD_DEFAULT=AUTHORIZE_GUILD_ENABLED


DEVELOPPER_MODE_DISABLED=0
DEVELOPPER_MODE_ENABLED=1
DEVELOPPER_MODE_DEFAULT=DEVELOPPER_MODE_DISABLED


# ============= COMMANDS AELF ==============
# Zone
AELF_ZONE_DEFAULT="france"

# Assets (legacy : hunfolding)
AELF_HUNFOLDING_RAW=0  # Legacy
AELF_HUNFOLDING_JSON=1  # Legacy
AELF_ASSETS_UNSET="" # --UNSET--
AELF_ASSETS_SET="./src/assets"
AELF_ASSETS_RESET="--RESET--"
AELF_ASSETS_DEFAULT=AELF_ASSETS_SET

AELF_ASSETS_ELEMENTS_DEFAULT=[
    {
        "key_name":"doxologie",
        "filename":"doxologie_short"
    },
    {
        "key_name":"pater_noster",
        "filename":"pater_noster_short"
    }
]


AELF_ASSETS_HUNFOLDING_RELATIVE_API="Offices/deroule"
AELF_ASSETS_HUNDOLDING_RELATIVE_FILE="offices"

AELF_ASSETS_CONTENT_RELATIVE_API="Bibliotheque?key="
AELF_ASSETS_CONTENT_RELATIVE_FILE="collections"

