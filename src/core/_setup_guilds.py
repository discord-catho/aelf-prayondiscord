import os
from dktotoolkit import write_message

def setup_guilds(self):
    """
    Récupère les paires (guild_id, user_id) à partir des variables d'environnement et les renvoie sous forme de liste de tuples.

    :return: Liste de tuples contenant les paires (guild_id, user_id)
    :rtypes: list

    :raise ValueError: Si la valeur de la variable d'environnement n'est pas un entier.
    """

    i_guild = -1

    data = {i_guild:{}}

    for key, value in os.environ.items():

        if not key.startswith(f"{self.options['prefix_environ']}_UPDATE_GUILD"):
            continue
        #

        if value.strip().isdigit():
            value = int(value.strip())
        else:
            raise ValueError(value)
        #

        number_guild = key.split("_")[-1].strip()
        if not number_guild.isdigit():
            raise ValueError(number_guild)
        #

        if not number_guild in data:
            data[number_guild] = {}
        #

        for elt in self.options['environ_variables_default'].keys():
            if elt in key.upper():
                data[str(number_guild)][elt] = value
            #
        #
    #

    if data:
        out = [
            {
                key:elt.get(key, default)
                for key, default
                in self.options['environ_variables_default'].items()
            }
            for elt in data.values()
            if elt.get("GUILD_ID",-1)>0
        ]

        if self.options["verbose"]:
            write_message(f"Found inside environ var: {out}", level="debug")
        #
        return out
    else:
        return []
    #
#
