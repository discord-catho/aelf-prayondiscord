import os, sys
import logging
import discord
from discord import app_commands, ui  # modals
from discord.ext import commands
from lib import DBGuilds



class dclient(commands.Bot):
    """Surcharger la classe commands.Bot afin que les modules (cogs et tasks) soient automatiquement chargés.

* Module cogs : slash commands
* Module taks : se lancent en fonction de l'horloge (todo : en fonction de réactions à un message)
"""

    txt_sep="-"*45+"\n"

    def __init__(self, prefix_environ="BOT"):
        sys.stdout.write(f"> discord.py version: {discord.__version__}\n")
        sys.stdout.write(self.txt_sep)

        """The constructor"""
        self.prefix_environ=prefix_environ

        self.logger = logging.getLogger(__name__) #"aelf-praywithdiscord.dclient")

        super().__init__(  # super() :  surcharge la classe commands.Bot
            command_prefix=commands.when_mentioned,
            intents=discord.Intents.all(),
            application_id=int(os.environ.get(f"{self.prefix_environ}_APPID")),
            strip_after_prefix=True,
        )

        self.synced = False  # Pour ne pas synchroniser plus d'une fois les fonctions

        self.initial_extensions=[
            "interactions.core_commands",
            "interactions.aelf_commands"
        ]
        # self.initial_extensions=[ toute la suite sera plutot pour des modules
        #     F"cogs.{filename[:-3]}"
        #     for filename in os.listdir('./cogs')  # cogs : commands
        #     if filename.endswith('.py')
        # ]+[
        #     F"tasks.{filename[:-3]}"
        #     for filename in os.listdir('./tasks') # tasks : at certain time
        #     if filename.endswith('.py')
        # ]+[
        #     F"cogs.{filename}"
        #     for filename in os.listdir('./cogs') # Directories
        #     if os.path.isdir(F"./cogs/{filename}") and filename[0:5] == "cogs_"
        # ]

        # Liste des noms d'événements à charger dynamiquement
        event_names = [
            "interactions.event.on_reaction_add",
            "interactions.event.on_command_error",
            # Ajoutez d'autres noms d'événements ici
        ]

        # Chargez dynamiquement les événements
        for event_name in event_names:
            self.setup_event(event_name)
        #
    #endDef

    async def on_ready(self):
        sys.stdout.write(self.txt_sep)
        sys.stdout.write(F"> Logged in as {self.user} (id: {self.user.id}) on {len(self.guilds)} guilds\n")
        for each_guild in self.guilds:
            sys.stdout.write(F"guilds> {each_guild.name} (id: {each_guild.id}, owner: {each_guild.owner})\n")
        #
        sys.stdout.write(self.txt_sep)
        await self.wait_until_ready()
        sys.stdout.write("> Bot is ready !\n")
        sys.stdout.write(self.txt_sep)

        sys.stdout.write("\n"+self.txt_sep)
        sys.stdout.write("> Database: update\n")

        with DBGuilds() as db:

            for each_guild in self.guilds:
                db.update_data(
                    table_name=db.options["table_guilds"],
                    data_dict={
                        "id_guild":each_guild.id,
                        "guild_name":each_guild.name
                    },
                    new_datas_col=["guild_name",],
                    commit=True
                )

                # Add if not inside table for aelf datas
                if db.get_datas(
                    table_name=db.options["table_aelf_guilds"],
                    conditions={"id_guild":each_guild.id},
                    if_present=True
                ) < 1:
                    db.insert_data(
                        table_name=db.options["table_aelf_guilds"],
                        data_dict={"id_guild":each_guild.id},
                        allow_duplicates=False,
                        commit=True)
                #
            #endFor

        #

        sys.stdout.write("> Database: ready\n")
        sys.stdout.write(self.txt_sep)
        sys.stdout.write("\n"+self.txt_sep)
        sys.stdout.write("All is ready: enjoy !\n")
        sys.stdout.write(self.txt_sep)
    #

    from ._setup_hook import setup_hook
    from ._setup_event import setup_event
    from ._close import close



#endClass
