import sys
import discord

from lib import DBGuilds

async def setup_hook(self)-> None:
    """Chargement du bot"""
    sys.stdout.write(self.txt_sep)
    sys.stdout.write(F"> Load extensions (cogs):\n")

    for ext in self.initial_extensions:
        sys.stdout.write(F"load cog> ")
        try:
            await self.load_extension(ext)
            sys.stdout.write("[loaded]  ")
        except Exception as e:
            sys.stdout.write("[errors]  ")
            print(e)
            raise
        #endTry
        sys.stdout.write(F"{ext}\n")
    #endFor
    self.logger.info(F"Load extensions (cogs): {self.initial_extensions}")

    sys.stdout.write(F"\n> Sync commands (cogs) on servers:\n")
    with DBGuilds() as db:
        for gid in db.get_guilds():
            sys.stdout.write(f"sync> ")
            try:
                sys.stdout.write("[ok]  ")
                await self.tree.sync(guild=discord.Object(id = int(gid)))
            except Exception as e:
                sys.stdout.write("[err]  ")
                print(e)
                raise Exception
            #endTry
            sys.stdout.write(f"{gid}\n")
        #endFor
    #endWith

    sys.stdout.write("> setup_hook : done\n")
    sys.stdout.write(self.txt_sep)
#endDef
