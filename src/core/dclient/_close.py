# Close bot and connections.
async def close(self) -> None:
    """Terminer l'exécution du bot proprement"""
    await super().close()
#endDef
