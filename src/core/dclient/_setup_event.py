import sys

def setup_event(self, event_name):
    """Charge un événement dynamiquement en fonction de son nom"""
    sys.stdout.write(self.txt_sep)
    sys.stdout.write(f"> Load event: {event_name}\n")

    try:
        name = event_name.split(".")[-1]
        event_module = __import__(event_name, fromlist=[name])
        event_function = getattr(event_module, name)
        self.add_listener(event_function)
        sys.stdout.write(f"load event> [loaded]  {event_name}\n")
    except Exception as e:
        sys.stdout.write(f"load event> [errors]  {event_name}\n")
        print(e)
        raise Exception
    #
    self.logger.info(F"Load events: {event_name}")
    sys.stdout.write(f"> setup_event : done ({event_name})\n")
    sys.stdout.write(self.txt_sep)
