import os
import logging

from dktotoolkit import write_message

from assets.__constants import AUTHORIZE_COMMAND_DEFAULT, AUTHORIZE_GUILD_DEFAULT
from .dclient import dclient
from lib import DBGuilds, classtodict

def _from_environ(name, **options):
    return os.environ.get(
        f"{options.get('prefix_environ')}_{name.upper()}",
        options.get(name, None)
    )

def _todict(obj):
    write_message("Please use: lib.classtodict", level="warning")
    return dict((k, getattr(obj, k)) for k in dir(obj) if not k.startswith('_'))


class Bot(object):
    class DefaultOptions:
        prefix_environ="BOT"
        token=None

        environ_variables_default={
            "GUILD_ID":-1,  # Id de la guilde (aka serveur)
            "GUILD_USERID":None,  # Id de l'utilisateur l'ayant installé (non fonctionnel)
            "GUILD_COMMAND_ALLOWED":AUTHORIZE_COMMAND_DEFAULT,  # Comportement par défaut : 1 = autoriser les interactions
            "GUILD_AUTHORIZED":AUTHORIZE_GUILD_DEFAULT,  # Autoriser la guild à utiliser le bot (non fonctionnel)
        }

        verbose=False

    class Options:
        pass

    def __init__(self, **options):

        self.options = classtodict(self.DefaultOptions)
        self.options.update(classtodict(self.Options))
        self.options.update(options)

        self.options["token"] = _from_environ(name="token", **self.options)

        self.logger = logging.getLogger(__name__)
    #

    async def __aenter__(self):
        '''
        Authorized usage 'async with Bot() as'
        '''

        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            print(f"Une exception de type {exc_type} dans async with a été levée : {exc_value}")
        else:
            print("Sortie du bloc async with sans exception")
        #
    #

    def __enter__(self):
        '''
        Authorized usage 'with Bot() as'
        '''

        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        '''
        Authorized usage 'with Bot() as'
        '''
        if exc_type is not None:
            print(f"Une exception de type {exc_type} a été levée : {exc_value}")
        else:
            print("Sortie du bloc with sans exception")
        #
    #

    from ._setup_guilds import setup_guilds

    async def run(self):
        self.logger.info("start bot")

        # Chercher les donnees de l'environnement d'execution
        try:
            update_guilds = self.setup_guilds()
            # 2023/09/13T13:28:59:DEBUG:discord.http.http.request,674:GET https://discord.com/api/v10/oauth2/applications/@me has received {'id': '1027198659324039189', 'name': 'Genevieve', 'icon': None, 'description': '', 'type': None, 'bot': {'id': '1027198659324039189', 'username': 'Genevieve', 'avatar': None, 'discriminator': '5724', 'public_flags': 0, 'flags': 0, 'bot': True, 'banner': None, 'accent_color': None, 'global_name': None, 'avatar_decoration_data': None, 'banner_color': None}, 'summary': '', 'bot_public': True, 'bot_require_code_grant': False, 'verify_key': '6007319f777ec708e170cb810312aef7973541e6c35fc4d529011b29e387cd06', 'flags': 2662400, 'hook': True, 'is_monetized': False, 'redirect_uris': [], 'interactions_endpoint_url': None, 'role_connections_verification_url': None, 'owner': {'id': '689912009118056498', 'username': 'pierredangle', 'avatar': 'e21477c461a49449464f9a2a29977564', 'discriminator': '0', 'public_flags': 0, 'flags': 0, 'banner': None, 'accent_color': None, 'global_name': "Pierre d'Angle", 'avatar_decoration_data': None, 'banner_color': None}, 'approximate_guild_count': 2, 'interactions_event_types': [], 'interactions_version': 1, 'explicit_content_filter': 0, 'rpc_application_state': 0, 'store_application_state': 1, 'creator_monetization_state': 1, 'verification_state': 1, 'integration_public': True, 'integration_require_code_grant': False, 'discoverability_state': 1, 'discovery_eligibility_flags': 2496, 'monetization_state': 1, 'monetization_eligibility_flags': 247140, 'team': None}

        except:
            raise
        #

        # MaJ la Bdd
        try:
            with DBGuilds(delete_if_error=True, verbose=self.options.get("verbose")) as db:
                for elt in update_guilds:
                    db.add_guild(
                        id_guild=elt["GUILD_ID"],
                        installer_id=elt["GUILD_USERID"],
                        authorized_guild=elt["GUILD_AUTHORIZED"],
                        restrict_members_usage=elt["GUILD_COMMAND_ALLOWED"],
                        update=False
                    )
                #
        except:
            write_message(f"Error while update database", level="critical")
            raise
        #

        try:
            async with dclient(prefix_environ=self.options.get("prefix_environ")) as client:

                await client.start(self.options.get("token"))

            #endWith
        except Exception as e:
            print(e)
            raise
        #
        self.logger.info("end bot")
    #


