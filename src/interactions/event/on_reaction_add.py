async def on_reaction_add(reaction, user):
    emoji = reaction.emoji

    # Si la reaction est celle d'un bot
    if user.bot:
        return
    #endIf

    if not reaction.message.author.bot :
        # si on reagit sur un humain
        return
    elif reaction.message.author.name == "Genevieve":
        print(F"Debug reaction: {reaction.emoji}")
    elif reaction.message.author.name != os.environ.get("BPRIERE_NAME"):
        # si on reagit sur un autre bot
        return
    #endIf

    if(reaction.message.reference is None):
        # les reactions sur l'integralite des messages ne me concernent pas : seul les votes m'interessent.
        return
    #endIf

    log_reaction="Reaction {s} on the message posted at {t} by the bot from {uu} : {c}".format(
        t=str(reaction.message.created_at),
        s=str(reaction),
        uu=str(reaction.message.reference.resolved.author),
        c=str(reaction.message.reference.resolved.content)
    )

    # write_log(user=user,
    #               content=(log_reaction),
    #               theFile=globals.logFile
    #               )


    fixed_channel = bot.get_channel(reaction.message.reference.channel_id)

    maintainer_id = await bot.fetch_user(globals.maintainer_id)

    if emoji == "\U0001F44E": #thumbs down :thumbsdown:
        await fixed_channel.send("Merci de faire un retour détaillé des choses à améliorer sur le bot à {} ;)".format(maintainer_id.mention))
    #endIf

# endDef
