async def on_command_error(ctx, error):  # Inutile a priori car je suis passe aux slash commands
    """Legacy"""
    if isinstance(error, commands.CommandNotFound):

        await ctx.send(F""":warning: La commande `{ctx.message.content}` n'existe pas :
- Vérifier l'orthographe
- Utiliser `{globals.command_prefix}sommaire`  ou  `{globals.command_prefix}help` pour voir la liste des commandes disponible
- Si vous pensez que cette commande devrait être implémentée, merci de lancer la commande  :
.\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0`{globals.command_prefix}request {ctx.message.content} (autres arguments s'il y en a) : précisions`""")

        # write_log(
        #     user=ctx.author,
        #     content="Command not found : \'{}\'".format(ctx.message.content),
        # )
        sys.stderr.write(str(error))

    else:

        await ctx.send(F""":warning: Mauvais usage de la commande `{ctx.message.content}` :
- Vérifier la documentation pour voir comment elle fonctionne
- Si vous pensez que cet usage devrait être implémenté, merci de lancer la commande  :
.\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0`{globals.command_prefix}request {ctx.message.content} (arguments s'il y en a) : précisions`
-Si vous pensez que c'est un bug, merci de le signaler avec `?breviaire_bug`""")

        # sys.stderr.write(str(ctx.command)+"\n"+str(ctx.message))
        # write_log(
        #     user=ctx.author,
        #     content=F"Error raised with the command `{ctx.message.content}`",
        # )

        raise error
    #endIf

    if "nones" in ctx.message.content:
        await ctx.send(F":arrow_right: Ne voulez-vous pas exécuter la commande `?none` (au singulier)  ?")
    elif "sixte" in ctx.message.content or "sixtes" in ctx.message.content or "sextes" in ctx.message.content:
        await ctx.send(F":arrow_right: Ne voulez-vous pas exécuter la commande `?sexte` (au singulier)  ?")
    #endIf

#endDef
