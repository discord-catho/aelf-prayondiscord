# Fichier : user/commands.py
# Fonction : Contient les commandes principales, destinées aux utilisateurs

_this_file_ = "cogs.cogs_office.__init__.py"
import os, sys

import discord
from discord.ext import commands
from discord import Interaction as discord_Interaction
from discord import app_commands

from discord.ext import commands

from typing import Optional, Union

from lib import CogsParent, available_guilds, check_permissions_database, load_translations
from core import _todict
from utils.office_to_discord import Office_To_Discord

commands=load_translations("fr_commands",nested_path="aelf/user/commands")

class CogsOfficeCommandsUser(CogsParent):

    class DefaultOptions:
        date = "today"

        # Decoupages
        discordwrap_width = 1020  # Dans les fields
        max_field_length = 4096  # Dans les embeds (ensemble de fields)
        max_nb_fields = 9  # Dans les embeds

        # View
        timeout = None  # After last interaction (seconds)
        timeout_beforedelete = 3600*24 # Suppression du message apres 1j
        pass

    class Options:
        discordwrap_width = 400
        discordwrap_item_group_paragraphs = False

        #max_field_length = 850
        max_field_length = 1536
        max_nb_fields = 5

        # POUR LES TESTS
        timeout = 59 # secondes < timeout_beforedelete
        timeout_beforedelete=60 # secondes
        pass

    def __init__(self, client, **options):

        super().__init__(client=client, **options)

        self.options.update(_todict(self.DefaultOptions))
        self.options.update(_todict(self.Options))
        self.options.update(options)

        if self.options["timeout_beforedelete"] <= self.options["timeout"]:
            self.options["timeout_beforedelete"] = self.options["timeout"] + 1
        #

    #endDef

    # ######################################################################## #
    #                           C O M M A N D S                                #
    # ######################################################################## #

    @app_commands.command(**commands["office"])
    @app_commands.guilds(*available_guilds())
    @app_commands.choices(office=[
        discord.app_commands.Choice(**e)
        for e in commands["office_choice"] if not "_README" in e.keys()
    ])
    @check_permissions_database(command="office")
    async def office(self,
                     interaction: discord_Interaction,
                     office: str,
                     date: Optional[str] = None
                     ) -> None:
        if date is None:
            date = self.options["date"]
        #

        await Office_To_Discord(**self.options).generate_office(
            interaction=interaction,
            name=office, date=date
        )
    #endDef

    # ######################################################################## #
    #                     E A C H   O F F I C E                                #
    # ######################################################################## #

    # TODO : la meme chose que les laudes pour les autres offices.
    @app_commands.command(**commands["laudes"])
    @app_commands.guilds(*available_guilds())
    @check_permissions_database(command="office")
    async def laudes(self, interaction: discord_Interaction) -> None:
        await Office_To_Discord(**self.options).generate_office(
            interaction=interaction,
            name="laudes",
            date=self.options["date"]
        )
    #endDef

    @app_commands.command(**commands["lectures"])
    @app_commands.guilds(*available_guilds())
    @check_permissions_database(command="office")
    async def lectures(self, interaction: discord_Interaction) -> None:
        await Office_To_Discord(**self.options).generate_office(interaction=interaction, name="lectures", date="today")
    #endDef

    @app_commands.command(**commands["tierce"])
    @app_commands.guilds(*available_guilds())
    @check_permissions_database(command="office")
    async def tierce(self, interaction: discord_Interaction) -> None:
        await Office_To_Discord(**self.options).generate_office(interaction=interaction, name="tierce", date="today")
    #endDef

    @app_commands.command(**commands["sexte"])
    @app_commands.guilds(*available_guilds())
    @check_permissions_database(command="office")
    async def sexte(self, interaction: discord_Interaction) -> None:
        await Office_To_Discord(**self.options).generate_office(interaction=interaction, name="sexte", date="today")
    #endDef

    @app_commands.command(**commands["none"])
    @app_commands.guilds(*available_guilds())
    @check_permissions_database(command="office")
    async def none(self, interaction: discord_Interaction) -> None:
        await Office_To_Discord(**self.options).generate_office(interaction=interaction, name="none", date="today")
    #endDef

    @app_commands.command(**commands["vepres"])
    @app_commands.guilds(*available_guilds())
    @check_permissions_database(command="office")
    async def vepres(self, interaction: discord_Interaction) -> None:
        await Office_To_Discord(**self.options).generate_office(interaction=interaction, name="vepres", date="today")
    #endDef

    @app_commands.command(**commands["complies"])
    @app_commands.guilds(*available_guilds())
    @check_permissions_database(command="office")
    async def complies(self, interaction: discord_Interaction) -> None:
        try:
            await Office_To_Discord(**self.options).generate_office(interaction=interaction, name="complies", date="today")
        except Exception as e:
            sys.stderr.write(f"!> {_this_file_} :  complie !\n{e}\n")
            raise
        #endTry
    #endDef

#endClass
