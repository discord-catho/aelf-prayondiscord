from discord.ext import commands

async def setup(client: commands.Bot):
    # Importer cogs_office:

    from .user import CogsOfficeCommandsUser
    from .admin import CogsOfficeParametersAdmin

    await client.add_cog(CogsOfficeCommandsUser(client))
    await client.add_cog(CogsOfficeParametersAdmin(client))
#endDef
