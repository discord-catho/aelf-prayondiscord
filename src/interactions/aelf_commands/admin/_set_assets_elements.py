import os
from dktotoolkit import write_message
from dktotoolkit.html import request_html_page
from requests.exceptions import ConnectionError

from lib import DBGuilds, load_translations
from assets.__constants import AELF_ASSETS_DEFAULT, AELF_ASSETS_ELEMENTS_DEFAULT, AELF_ASSETS_CONTENT_RELATIVE_API, AELF_ASSETS_CONTENT_RELATIVE_FILE

commands=load_translations("fr_commands",nested_path="aelf/admin")

def _init_set_assets_elements(guild_id):
    with DBGuilds() as db:
        dev_mode = db.get_datas(
            table_name=db.options["table_guilds"],
            fields=["developper_mode",],
            conditions={"id_guild":guild_id},
        )[0][0]
        assets_path = db.get_datas(
            table_name=db.options["table_aelf_guilds"],
            fields=["use_hunfolding",],
            conditions={"id_guild":guild_id},
        )[0][0]
    #

    return dev_mode, assets_path
#

    # if :

    #     filename = [e["filename"] for e in AELF_ASSETS_ELEMENTS_DEFAULT if e["key_name"].lower()==key_name.lower()]

    #     if len(filename) > 1:
    #         write_message(f"Warning : expected length=1 : filename={filename}", level="warning")
    #     #
    #     filename = filename[0]

    # #

def _set_assets_elements_reset(guild_id:int, not_dev_mode:bool, reset_all:bool, reset_keyname:str=None):
    '''
    Reset tous les elements si on n'est pas dev si reset_not_dev
    '''


    if reset_all:

        reset_elements = AELF_ASSETS_ELEMENTS_DEFAULT
        conditions_reset={
            "guild_id":guild_id,
        }

        message = commands["set_assets_elements_msg_devmod_off"]

    elif not_dev_mode:

        return commands["set_assets_elements_msg_devmod_off_notreset"]

    elif reset_keyname is not None:
        filename_tmp = [e for e in AELF_ASSETS_ELEMENTS_DEFAULT if e["key_name"].lower()==reset_keyname.lower()]

        if len(filename_tmp) > 1:
            write_message(f"Warning : expected length=1, so reset only first item : filename={filename_tmp}", level="warning")
        #
        reset_elements = [[e["filename"] for e in filename_tmp][0],] if len(filename_tmp) > 0 else []
        conditions_reset={
            "guild_id":guild_id,
            "key_name":  reset_keyname
        }

        message = f"reset {conditions_reset['key_name']}"
    #

    with DBGuilds() as db:
        kwargs_insert = {
            "table_name":db.options["table_aelf_assets_elements"],
            "new_datas_col":["filename",],
            "commit":True
        }

        db.delete_data(
            table_name=db.options["table_aelf_assets_elements"],
            conditions=conditions_reset,
            commit=True
        )

        for elt in reset_elements:
            data_dict={
                "guild_id":guild_id,
            }
            data_dict.update(elt)

            db.insert_data(
                table_name=db.options["table_aelf_assets_elements"],
                data_dict=data_dict,
                allow_duplicates=False,
                commit=True
            )
        #
    #

    return message
#

def _check_filename_api(assets_path, filename):

    path_api = os.path.join(assets_path, AELF_ASSETS_CONTENT_RELATIVE_API)+filename

    # Tester si path_api existe

    try:
        content, status = request_html_page(
            url=path_api,
            format_output="json",
            output_status=True
        )
        status_api = content.get("status", 404)
        path_found = (int(status) == 200) and  (int(status_api) == 200)
    except ConnectionError:
        path_found = False
    except TypeError:
        write_message(f"TypeError : html_status={status}", level="critical")
        raise
    except Exception:
        write_message(f"Unkown Error !", level="critical")
        raise
    #

    return path_api, path_found
#

def set_assets_elements(
        guild_id:int,
        key_name: str,
        filename: str,
        reset_filename:int,
        reset_all:int,
        force_add:int,
        **kwargs
):

    dev_mode, assets_path =  _init_set_assets_elements(guild_id=guild_id)

    if (not dev_mode) or reset_all or reset_filename:
        return _set_assets_elements_reset(
            guild_id = guild_id,
            not_dev_mode = not dev_mode,
            reset_all = reset_all,
            reset_keyname = key_name
        )
    #

    path_api = None
    path_found = False
    if assets_path[:4].lower() == "http":
        path_api, path_found = _check_filename_api(assets_path, filename)
    #

    path_file = os.path.join(AELF_ASSETS_DEFAULT,AELF_ASSETS_CONTENT_RELATIVE_FILE, filename+".json")

    path_found = path_found or os.path.exists(path_file)

    if path_found or force_add:

        element = {
            "guild_id":guild_id,
            "key_name":key_name.lower(),
            "filename":filename.lower()
        }

        with DBGuilds() as db:
            db.insert_data(
                table_name=db.options["table_aelf_assets_elements"],
                data_dict=element,
                replace_on_datas=["guild_id", "key_name"],
                allow_duplicates=False,
                commit=True
            )
        #

        message = commands["set_assets_elements_msg_devmod_on"]
        message += f" {key_name}: {filename}"

    else:

        message = f"Not found:\n- File: [{path_file}](file://{os.path.abspath(path_file)})\n- API: {path_api}"

    #

    return message
#
