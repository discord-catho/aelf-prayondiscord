import discord
from lib import DBGuilds


class Modal_Maintainer_Reset_Officehunfolding_HTML(
        discord.ui.Modal,
        title='DEV MODE: API déroulement'
):

    name = discord.ui.TextInput(label="Entrez un lien vers l\'API", placeholder="https://api.sub.domain.ext/v1/  !without officename!")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    #

    async def on_submit(self, interaction: discord.Interaction):
        defaultdomain = "localhost"
        defaultport = 8000
        defaulthttp = "http" # https
        if len(str(self.name.value)) > 4:

            url = self.name.value

            if url[0]=="/":
                url = f"{defaulthttp}://{defaultdomain}:{defautport}/{url}"
            #
            elif not "http" in url[:5]:
                url = f"{defaulthttp}://{url}"
            #

            with DBGuilds() as db:
                db.update_data(
                    table_name=db.options["table_aelf_guilds"],
                    data_dict={
                        "id_guild":interaction.guild.id,
                        "use_hunfolding":url
                    },
                    new_datas_col=["use_hunfolding",],
                    commit=True
                )
            await interaction.response.send_message(f'Thanks for your response, hunfolding link: {url}!', ephemeral=True)

        else:

            raise ValueError(f"setofficehunfolding: {self.name.value} too short !")
        #
    #
#
