# Fichier : admin/parameters.py
# Fonction : Contient les commandes pour modifier des paramètres, destinées aux administrateurs
from typing import Optional, Union

import discord
from discord import app_commands
from discord import Interaction as discord_Interaction
from discord.app_commands import Choice

from dktotoolkit import write_message

from core import _todict
from lib import CogsParent, available_guilds, DBGuilds, load_translations

from utils.aelf import AELF

from assets.__constants import AELF_ASSETS_DEFAULT, AELF_ASSETS_SET, AELF_ASSETS_RESET, AELF_ASSETS_UNSET

from ._modal_reset_officehunfolding_confirmation import Modal_Reset_Officehunfolding_Confirmation
from ._set_assets_elements import set_assets_elements

commands=load_translations("fr_commands",nested_path="aelf/admin")

class CogsOfficeParametersAdmin(CogsParent):

    class DefaultOptions:
        office_hunfolding = AELF_ASSETS_DEFAULT

    class Options:
        pass


    def __init__(self, client, **options):

        super().__init__(client=client, **options)

        self.options.update(_todict(self.DefaultOptions))
        self.options.update(_todict(self.Options))
        self.options.update(options)
    #


    @app_commands.command(**commands["sethunfolding"])
    @app_commands.checks.has_permissions(administrator=True)
    @app_commands.guilds(*available_guilds())
    @app_commands.choices(set_value=[
        Choice(**e)
        for e in commands["sethunfolding_choice"]
    ])
    async def sethunfoldhing(self, interaction: discord_Interaction,
                             set_value: str,
                            ) -> None:
        """
        Choisir le déroulement (mise à jour de la base de donnée pour le serveur sur lequel la commande est lancée)

        :param str set_value:

        * '0' = Utiliser les données brutes d'AELF
        * '1' = "Bréviaire minimal" : Ajout doxologie + répétition des antiennes
        * 'reset' = Remise à zéro du paramètre, ouvre un modal de confirmation
        """
        #todo : faire discrimination entre membre et role

        if set_value in [AELF_ASSETS_SET, AELF_ASSETS_UNSET]:

            self.options['office_hunfolding'] = set_value

            with DBGuilds() as db:
                db.update_data(
                    table_name=db.options["table_aelf_guilds"],
                    data_dict={
                        "id_guild":interaction.guild.id,
                        "use_hunfolding":set_value
                    },
                    new_datas_col=["use_hunfolding",],
                    commit=True
                )
            #

        elif set_value == AELF_ASSETS_RESET:

            modal = Modal_Reset_Officehunfolding_Confirmation(self)
            await interaction.response.send_modal(modal)

        # if value == oui: ajouter a la bdd que hunfolding = "0" (cad ne pas utiliser un deroule)
        # if value == non: ajouter a la bdd que hunfolding = "1" (cad utiliser le deroule json)
        # if value == reset: demander confirmation reset (avec champs texte) : si "yes"/"oui"/"y"/"o" : passer a 0 dans la bdd, si "no"/"n" ne rien faire, si url : ajouter url a bdd

        # TODO : ajouter a BDD

        # TODO : app_commands.check(fonction_perso) avec fonction_perso qui verifie
        # - guilde est bonne
        # - et (commande est bon ou commande est -1)
        # - et (   utilisateur et channel et commande sont bons si utilisateur et channel
        # -      ou channel est bon si utilisateur (None, -1, 0)
        # -      ou utilisateur est bon si channel (None, -1, 0)
        # -      ou utilisateur est administrateur
        # -      ou utilisateur est owner du bot
        #      )

        await interaction.response.send_message(f'Thanks for your response, {set_value}!', ephemeral=True)
    #endDef

    @app_commands.command(**commands["set_assets_elements"])
    @app_commands.checks.has_permissions(administrator=True)
    @app_commands.guilds(*available_guilds())
    @app_commands.choices(reset_filename=[
        Choice(**e)
        for e in commands["set_assets_elements_reset_choice"]
    ])
    @app_commands.choices(reset_even_not_dev=[
        Choice(**e)
        for e in commands["set_assets_elements_reset_choice"]
    ])
    @app_commands.choices(force_add=[
        Choice(**e)
        for e in commands["set_assets_elements_reset_choice"]
    ])
    async def setassets(self, interaction: discord_Interaction,
                        key_name: str,
                        filename: str="",
                        force_add:int=0,
                        reset_filename:int=0,
                        reset_even_not_dev:int=0,
                        ) -> None:
        """
        Pour le mode développeur, afin d'utiliser une API pour ne pas avoir à redémarrer le bot
        """

        if filename and reset_filename:
            message = "Impossible d'avoir *et* filename *et* reset_filename !"
        else:
            message = set_assets_elements(
                guild_id = interaction.guild.id,
                key_name = key_name,
                filename = filename,
                reset_filename = reset_filename,
                reset_all = reset_even_not_dev,
                force_add = force_add
            )
        #
        await interaction.response.send_message(message, ephemeral=True)

    #

    @app_commands.command(**commands["set_zone"])
    @app_commands.checks.has_permissions(administrator=True)
    @app_commands.guilds(*available_guilds())
    @app_commands.choices(zone=[
        Choice(**{
            "name":each_zone.capitalize(),
            "value":each_zone
        })
        for each_zone in AELF.DefaultOptions.available_zone
    ])
    async def setzone(
            self,
            interaction: discord_Interaction,
            zone:str
    ) -> None:


        with DBGuilds() as db:
            db.update_data(
                table_name=db.options["table_aelf_guilds"],
                data_dict={
                    "id_guild":interaction.guild.id,
                    "zone":zone,
                    },
                    new_datas_col=["zone",],
                    commit=True
                )
        #
        try:
            await interaction.response.send_message(
                commands["set_zone_message"]+f' {zone}',
                ephemeral=True
            )
        except Exception as e:
            print(e)
            raise
    #endDef
#
