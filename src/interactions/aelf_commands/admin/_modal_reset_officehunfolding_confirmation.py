import os
import discord

from lib import DBGuilds, Modal_Open_Private_Mode
from assets.__constants import  AELF_ASSETS_DEFAULT
from ._modal_reset_officehunfolding_maintainer_mode import Modal_Maintainer_Reset_Officehunfolding_HTML

class Modal_Reset_Officehunfolding_Confirmation(
        Modal_Open_Private_Mode,
        title='Validation remise à zéro',
        txt_label="Remettre la valeur par défaut ? ('oui'/'non')"
):

    def __init__(self, cogs_office, *args, **kwargs):
        self.cogs_office = cogs_office
        super().__init__(
            modal=Modal_Maintainer_Reset_Officehunfolding_HTML(),
            *args,
            **kwargs
        )
    #

    async def on_submit(self, interaction: discord.Interaction):

        good=False

        if str(self.name.value).lower() in ['oui', 'o', 'yes', 'y']:

            self.cogs_office.options["office_hunfolding"]=AELF_ASSETS_DEFAULT
            with DBGuilds() as db:
                db.update_data(
                    table_name=db.options["table_guilds"],
                    data_dict={
                        "id_guild":interaction.guild.id,
                        "use_hunfolding":AELF_ASSETS_DEFAULT
                    },
                    new_datas_col=["use_hunfolding",],
                    commit=True
                )
                await interaction.response.send_message(
                    f'Remis à zéro {AELF_ASSETS_DEFAULT} !',
                    ephemeral=True
                )
            #
            good=True

        elif str(self.name.value).lower() in ['non', 'no', 'n']:

            await interaction.response.send_message(f'Not modified !', ephemeral=True)

            good=True
        #

        elif str(self.name.value) == "'oui'/'non'":

            await interaction.response.send_message(
                f'Petit malin :stuck_out_tongue_closed_eyes: Pour t\'apprendre, tu auras à recommencer !',
                ephemeral=True
            )
            good=True
        #

        # Pour verifier la clé
        if await super().on_submit(interaction=interaction):
            good = True
        #

        if not good:
            await interaction.response.send_message(
                'Erreur, il faut écrire en toute lettres \'oui\'/\'non\'',
                ephemeral=True
            )
            raise ValueError(f"setofficehunfolding: {self.name.value} not in oui/non")
        #
    #
#


