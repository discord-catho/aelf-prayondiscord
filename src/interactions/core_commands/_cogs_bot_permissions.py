import discord
from discord.ext import commands
from ..utils.permissions.check_permissions import check_admin_permissions

'''
TODO
Potentiellement pas mal a degager ici...
'''
class CogsBotPermissions(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def setpermissions(self, ctx, command_name, user_or_group=None, channel=None):
        # Vérifier si l'utilisateur est administrateur
        if not check_admin_permissions(ctx.author):
            await ctx.send("Vous n'avez pas la permission d'utiliser cette commande.")
            return

        # Vérifier que l'un des champs optionnels est rempli
        if user_or_group is None and channel is None:
            # Aucun champ optionnel n'est rempli, demander confirmation
            confirmation = await ctx.send("Aucun champ optionnel n'est rempli. Êtes-vous sûr de vouloir réinitialiser les permissions ? Répondez avec 'oui' pour confirmer.")

            def check(response):
                return response.author == ctx.author and response.content.lower() == 'oui'

            try:
                response = await self.bot.wait_for('message', check=check, timeout=30)
            except asyncio.TimeoutError:
                await ctx.send("Aucune confirmation reçue, la réinitialisation a été annulée.")
                return

        # Code pour gérer les permissions ici
        # Vous pouvez utiliser les arguments command_name, user_or_group, et channel pour effectuer les opérations nécessaires

        await ctx.send(f"Permissions mises à jour pour la commande '{command_name}'.")

def setup(bot):
    bot.add_cog(PermissionsCog(bot))
