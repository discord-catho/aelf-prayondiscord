# Fichier : admin/parameters.py
# Fonction : Contient les commandes pour modifier des paramètres, destinées aux administrateurs

from typing import Optional, Union

import discord
from discord import app_commands
from discord import Interaction as discord_Interaction

from dktotoolkit import write_message

from core import _todict
from lib import DBGuilds

from lib import CogsParent, available_guilds, load_translations

from lib.view_developper_mode import View_Developper_Mode_Activate

commands=load_translations("fr_commands",nested_path="admin")

class CogsBotParametersAdmin(CogsParent):

    def __init__(self, client, **options):

        super().__init__(client=client, **options)

        self.options.update(options)
    #

    @app_commands.command(**commands["set_developper_mode"])
    @app_commands.checks.has_permissions(administrator=True)
    @app_commands.guilds(*available_guilds())
    async def set_developper_mode(
            self,
            interaction: discord_Interaction,

    ) -> None:
        """
        1. on envoie le message:
            - [demande autorisation]   (bouton, actif)
            - ((oui), (non), (rester)) (liste,  inactif)
            - [annuler]                (bouton, actif)    NON FONCTIONNEL
            - [valider]                (bouton, inactif)

        2. on clic sur [demande autorisation]  : ouverture modal pour y entrer sa clé publique

        3a le modal (aka la clé publique) est bon:
            - activer la liste et le bouton valider
            - desactiver le bouton d'ouverture du modal

        3b le modal n'est pas bon (ValueError):
           - ne rien faire (on devrait pouvoir avoir accès aux boutons)
        """

        activated = None
        with DBGuilds() as db:
            activated = int(db.get_datas(
                table_name=db.options["table_guilds"],
                conditions={
                    "id_guild":interaction.guild.id,
                },
                fields=["developper_mode",]
            )[0][0]) > 0
        #
        write_message("A deplacer dans cogs/cogs_base/admin", level="debug")
        view = View_Developper_Mode_Activate(interaction)
        await interaction.response.send_message(f"Status: {activated}\nCommencez par recuperer l'autorisation", view=view, ephemeral=True)
    #endDef


    @app_commands.command(**commands["setpermissions"])
    @app_commands.checks.has_permissions(administrator=True)
    @app_commands.guilds(*available_guilds())
    @app_commands.rename(the_member_or_group='membre_ou_groupe')
    @app_commands.choices(default_comportement=[
        discord.app_commands.Choice(**e) for e in commands["setpermissions_choice"]
    ])
    async def setpermissions(self, interaction: discord_Interaction,
                             default_comportement:int=0,
                             the_member_or_group: Union[discord.Member, discord.Role]=None,
                             the_channel:Union[discord.TextChannel, discord.ForumChannel, discord.Thread]=None,
                             ) -> None:
        '''
        the_channel : si none, alors tous les channels
        '''
        # Gerer les channels
        if the_channel is not None:
            channel_id = the_channel.id
        else:
            # channel_id = interaction.channel_id  # channel actuel
            channel_id = None
        #

        user_id = None
        role_id = None
        if the_member_or_group is None:
            write_message("TODO: modifier la construction de la bdd pour que dans permission_commands, par defaut, role_id soit le groupe everyone", level="debug")
            member_group_id = interaction.guild_id  # cad le role everyone
        elif isinstance(the_member_or_group, discord.Member):
            user_id = the_member_or_group.id
        elif isinstance(the_member_or_group, discord.Role):
            role_id = the_member_or_group.id
        #

        command_name = "office"
        with DBGuilds() as db:
            try:
                db.insert_data(
                    table_name=db.options["table_permissions_commands"],
                    data_dict={
                        "guild_id":interaction.guild.id,
                        "channel_id":channel_id,
                        "user_id":user_id,
                        "role_id":role_id,
                        "command_name":command_name,
                        "allowed":default_comportement
                    },
                    replace_on_datas=["allowed",],
                    reverse_replace_on_datas=True,
                    allow_duplicates=False,
                    commit=True,
                    verbose=self.options["verbose"]
                )
            except Exception as e:
                print(e)
                write_message(f"Problème dans la mise à jour de c{channel_id} r{role_id} u{user_id} : {command_name}", level="critical")
                raise
            #
        #
        try:
            await interaction.response.send_message(f'Coucou {the_member_or_group} // {the_channel}', ephemeral=True)
        except Exception as e:
            print(e)
            raise
    #endDef
