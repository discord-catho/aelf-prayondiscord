from discord.ext import commands

async def setup(client: commands.Bot):

    # Importer les commandes de base du bot

    from ._cogs_bot_parameters_admin import CogsBotParametersAdmin
    await client.add_cog(CogsBotParametersAdmin(client))

    # from ._cogs_bot_permissions import CogsBotPermissions
    # client.add_cog(CogsBotPermissions)

#endDef
