import logging

from discord.ext import commands

from lib import DBGuilds, check_permissions_database
from core import _todict


with DBGuilds() as db:
    _available_guilds = db.get_guilds()
#


class CogsBot(commands.Cog):

    options = {
        "guilds":[],
        "roles":[]
    }

    class DefaultOptions:
        guilds = []
        roles = []

    class Options:
        pass

    def __init__(self, client, **options):

        self.options.update(_todict(self.DefaultOptions))
        self.options.update(_todict(self.Options))
        self.options.update(options)

        self.client = client

        self.logger = logging.getLogger(__name__)
    #endDef

#endClass
