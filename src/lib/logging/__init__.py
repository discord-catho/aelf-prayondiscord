import os
import logging

class CustomLogger:

    class BaseFilter(logging.Filter):
        def filter(self, record):
            raise NotImplementedError
        #
    #

    class NameStartsWithFilter(BaseFilter):
        def __init__(self, *startswith):
            """
            Keep only elements mentionned

            :param str startswith: str, str, str (withoutbrackets)
            """
            self.startswith = startswith
        #

        def filter(self, record):
            return  any(record.name.startswith(f'{elt}.') for elt in self.startswith)
        #
    #
    class ExcludeNamesFilter(BaseFilter):
        def __init__(self, *excluded_names):
            """
            Exclude all elements mentionned

            :param str excluded_names: Elements, NOT AS LIST, but str, str, str (without brackets)
            """

            self.excluded_names = excluded_names
        #
        def filter(self, record):
            return not any(record.name.startswith(f'{elt}.') for elt in self.excluded_names)
        #

    #

    @staticmethod
    def create_handler(file_name, formatter, filter=None):
        """
        Create a logging handler.

        :param str file_name: Name of the log file.
        :param logging.Formatter formatter: Formatter for the log messages.
        :param logging.Filter filter: Optional filter for the handler.
        :return: Created handler.
        :rtype: logging.Handler
        """
        handler = logging.FileHandler(file_name)
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)

        if filter:
            handler.addFilter(filter)

        return handler

    @classmethod
    def setup_logger(cls, reset_logs=False, logs_directory="logs"):
        """
        Set up the logger.

        :param bool reset_logs: Whether to reset logs.
        """
        formatter = logging.Formatter(fmt=f'%(asctime)s:%(levelname)s:%(name)s.%(module)s.%(funcName)s,%(lineno)d:%(message)s', datefmt='%Y/%m/%dT%H:%M:%S')

        # Configurer le logger principal
        main_logger = logging.getLogger()  # Attention : laisser vide ici !
        main_logger.setLevel(logging.DEBUG)

        # Liste des handlers
        handlers = []

        os.makedirs(logs_directory, exist_ok=True)

        if reset_logs:
            test_file="w"
        else:
            test_file="a"
        try:
            # Vérifier si vous avez les permissions pour écrire dans les fichiers
            for log_name in ["bot.log", "discord.log", "other.log"]:
                with open(os.path.join(logs_directory, log_name), test_file) as f:
                    pass
                #
            #

            # Créer de nouveaux handler pour le fichier
            handler = cls.create_handler(
                os.path.join(logs_directory, "bot.log"),
                formatter,
                cls.NameStartsWithFilter("core")
            )
            handlers.append(handler) # Ajouter le handler à la liste

            handler = cls.create_handler(
                os.path.join(logs_directory, "discord.log"),
                formatter,
                cls.NameStartsWithFilter("discord")
            )
            handlers.append(handler) # Ajouter le handler à la liste

            handler = cls.create_handler(
                os.path.join(logs_directory, "other.log"),
                formatter,
                cls.ExcludeNamesFilter("discord", "core")
            )
            handlers.append(handler) # Ajouter le handler à la liste

        except OSError:
            # Si vous n'avez pas les permissions, le gestionnaire de console est activé
            console_handler = logging.StreamHandler()
            console_handler.setLevel(logging.DEBUG)
            console_handler.setFormatter(formatter)
            main_logger.addHandler(console_handler) # Ajouter les gestionnaires au logger principal

            main_logger.error("Can not write files, use console !")

        # Ajouter tous les handlers à logger principal
        for handler in handlers:
            main_logger.addHandler(handler)
        #
