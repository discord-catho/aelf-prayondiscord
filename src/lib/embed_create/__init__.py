# https://discord.com/developers/docs/resources/channel#embed-object
# https://discordpy.readthedocs.io/en/stable/api.html#embed

from discord import Embed
def __init__():
    pass



def create_embed(fields:list=[], max_field_length:int = 2040, max_nb_fields=10, **kwargs):
    '''
    fields = [{"name", "value", "inline"}, ...]
    '''

    embed_base = Embed(
        color=kwargs.get("color", None),
        title=kwargs.get("title", None),
        type=kwargs.get("type", 'rich'),
        url=kwargs.get("url", None),
        description=kwargs.get("description", None),
        timestamp=kwargs.get("timestamp", None)
    )

    embed_base.set_author(
        name=kwargs.get("author_name", None),
        url=kwargs.get("author_url", None),
        icon_url=kwargs.get("author_icon_url", None)
    )

    embed_base.set_image(
        url=kwargs.get("image_url", None)
    )

    embed_base.set_thumbnail(
        url=kwargs.get("thumbnail_url", None)
    )

    embed_base.set_footer(
        text=kwargs.get("footer_text", None),
        icon_url=kwargs.get("footer_icon_url", None)
    )


    len_fields = sum([len(str(e.get("value", ""))) for e in fields])

    if len_fields < max_field_length:
        for each_field in fields:
            embed_base.add_field(**each_field)
        #
        return embed_base
    else:
        d = embed_base.to_dict()
        L = []

        tmp_field_length = 0
        tmp_nb_fields = 0

        i = 0
        for each_field in fields:

            same_page = each_field.get("same_page", False)
            force_same_page = each_field.get("force_same_page", False)
            del each_field["same_page"]
            del each_field["force_same_page"]

            if not (same_page or force_same_page):
                L += [Embed().from_dict(d),]
                tmp_field_length = 0
                tmp_nb_fields = 0
            elif ( (
                    tmp_field_length + len(str(each_field.get("value", ""))) > max_field_length or
                    tmp_nb_fields >= max_nb_fields
            )and not force_same_page ):
                L[-1].add_field(
                    **{
                        "name": "" ,
                        "value": "_ _"+60*" "+"▶",
                        "inline": False
                    }
                )
                #
                L += [Embed().from_dict(d),]
                tmp_field_length = 0
                tmp_nb_fields = 0
            elif not L:
                L = [Embed().from_dict(d),]
                tmp_field_length = 0
                tmp_nb_fields = 0
            #

            L[-1].add_field(**each_field)
            tmp_nb_fields += 1
            tmp_field_length += len(str(each_field.get("value", "")))

            i+= 1
        #

        return L
    #
    raise Exception
#
