import os
from dktotoolkit import check_private_key, write_message

from ..db_guilds import DBGuilds
from ..load_translations import load_translations

from ._view_button_private_modal import View_Button_Open_Private_Modal
from ._check_key import check_rsa_key

async def call_button_open_private_modal(
        interaction,
        modal,
        force_key=False,
        **kwargs
):
    txt = load_translations("fr", nested_path="core/rsa_key")

    check = check_rsa_key(
        guild_id=interaction.guild.id,
        force_key=force_key,
        **kwargs
    )

    if check and modal is not None:

        await interaction.response.send_message(txt["developer_mode"], view=View_Button_Open_Private_Modal(modal=modal), ephemeral=True)

    elif modal is not None:

        await interaction.response.send_message(txt["developer_mode_wrong_key"], ephemeral=True)

    elif check:

        return True

    else:

        return False
    #
#
