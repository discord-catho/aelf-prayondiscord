import os
from dktotoolkit import check_private_key, write_message

from ..db_guilds import DBGuilds

def check_rsa_key(
        guild_id,
        environ_private_key,
        environ_private_key_path,
        force_key=False,
        **kwargs
):
    write_message("todo: Documentation de tout le sous module", level='debug')
    check = False

    if not force_key:
        with DBGuilds() as db:
            check = int(db.get_datas(
                table_name=db.options["table_guilds"],
                conditions={
                    "id_guild":guild_id,
                },
                fields=["developper_mode",]
            )[0][0]) > 0
        #
    #

    if (
            not check and
            (os.environ.get(environ_private_key_path) or
             os.environ.get(environ_private_key) ) ):
        # Regarder si les cles correspondent
        try:
            check = check_private_key(
                environ_private_key=environ_private_key,
                environ_private_key_path=environ_private_key_path,
                **kwargs
            )
        except Exception as e:
            write_message(e, level="critical")
            raise
        #
    #

    return check
#
