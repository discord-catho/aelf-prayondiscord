import os
import discord

from ..load_translations import load_translations

from ._view_button_private_modal import View_Button_Open_Private_Modal
from ._call_button_open_private_modal import call_button_open_private_modal
from ._check_key import check_rsa_key

def txt(var):

    if globals()["__package__"].split(".")[0] == "aelf_prayondiscord_as_module":
        return ""
    #

    return load_translations("fr", nested_path="core/rsa_key")[var]
#

class Modal_Open_Private_Mode(
        discord.ui.Modal,
        title='Open administration mode',
):
    '''
    Classe avec une seule entrée, qui est la clé du modal (à surcharger pour d'autres fonctions)
    '''

    txt_label=''
    txt_min_length=0
    name = discord.ui.TextInput(
        label=txt_label,
        min_length=txt_min_length,
        required=True,
        style=discord.TextStyle.paragraph
    )

    def __init_subclass__(cls,
                          txt_label=txt("enter_public_key"),
                          txt_min_length=1,
                          **kwargs):

        cls.txt_label = txt_label
        cls.txt_min_length = txt_min_length

        cls.name = discord.ui.TextInput(
            label=cls.txt_label,
            min_length=cls.txt_min_length,
            required=True,
            style=discord.TextStyle.paragraph
        )

        super().__init_subclass__(**kwargs)
    #



    def __init__(self, modal=None, txt_label=txt("enter_public_key"), *args, **kwargs):
        self.modal = modal
        self.txt_label = txt_label
        # self.txt_label = txt_label
        # self.txt_min_length = txt_min_length
        super().__init__(*args, **kwargs)
    #

    async def on_submit(self, interaction: discord.Interaction, force_key=False)->bool:

        try:
            if await call_button_open_private_modal(
                    interaction=interaction,
                    modal=self.modal,
                    public_key=self.name.value,
                    environ_private_key="BOTAELF_DEVELOPMENTMODE_PRIVATE_RSAKEY",
                    environ_private_key_path="BOTAELF_DEVELOPMENTMODE_PRIVATE_RSAKEY_PATH",
                    environ_private_passwd='BOTAELF_DEVELOPMENTMODE_PUBLIC_RSAKEY_PASSWD',
                    force_key=force_key
            ):
                return True
            #
        except Exception as e:
            print(e)
            raise
        #

        await interaction.response.send_message(
            txt("wrong_key"),
            ephemeral=True
        )
        return False
        #
    #
#

