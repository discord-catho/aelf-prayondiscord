import discord
class View_Button_Open_Private_Modal(discord.ui.View):
    def __init__(self, modal, **kwargs):
        self.modal=modal
        super().__init__(**kwargs)
    #

    @discord.ui.button(label="Open private modal", style = discord.ButtonStyle.primary, row=0)
    async def buttonFirst(self, interaction:discord.Interaction, button: discord.ui.Button):
        self.symbol = None
        await interaction.response.send_modal(self.modal)
        await modal.wait()
    #endDef
