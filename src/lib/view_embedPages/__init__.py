import discord

class publishEmbeds(discord.ui.View):

    hidden_options = ["end_message",]

    def __init__(self, embedPages=None, user=None, **kwargs):

        self.options = {}
        for k in self.hidden_options:
            self.options[k] = kwargs.get(k, None)
            del kwargs[k]
        #

        super().__init__(**kwargs)

        # Utile
        self.currentPage = 0 if embedPages is not None else None
        self.maxPages = len(embedPages) if embedPages is not None else None
        self.symbol=None
        self.embedPages=embedPages

        self.user=user

    #endDef

    async def turnPage(self, interaction, increment=0, fast=0)->None:

        if self.currentPage is None:
            interaction.response.edit_message(content="Bug ? Anything to display", embed=None, view=None)
        #endIf

        if fast == 1:
            self.currentPage = self.maxPages
        elif fast == -1:
            self.currentPage = 0
        else:
            self.currentPage += increment
        #endIf

        if self.currentPage < 0:
            self.currentPage = 0
        elif self.currentPage >= self.maxPages :
            self.currentPage = self.maxPages-1
        #endIf

        try:

            await interaction.response.edit_message(
                content = f"{self.currentPage+1}/{self.maxPages}",
                embed=self.embedPages[self.currentPage],
                view=self
            )

        except Exception as e:

            raise

        #endTry
    #endDef


    @discord.ui.button(label = "⏮", style = discord.ButtonStyle.secondary, row=0)
    async def buttonFirst(self, interaction:discord.Interaction, button: discord.ui.Button):
        self.symbol = "⏮"
        await self.turnPage(interaction=interaction, fast=-1)
    #endDef


    @discord.ui.button(label = "◀", style = discord.ButtonStyle.secondary, row=0)
    async def buttonPrevious(self, interaction:discord.Interaction, button: discord.ui.Button):
        self.symbol = "◀"
        await self.turnPage(interaction=interaction, increment=-1)
    #endDef


    @discord.ui.button(label = "▶", style = discord.ButtonStyle.success, row=0)
    async def buttonNext(self, interaction:discord.Interaction, button: discord.ui.Button):
        self.symbol = "▶"
        await self.turnPage(interaction=interaction, increment=1)

        if self.currentPage >= self.maxPages - 2:
            button.style = discord.ButtonStyle.secondary
        else:
            button.style = discord.ButtonStyle.success
        #
    #endDef

    @discord.ui.button(label = "⏭", style = discord.ButtonStyle.secondary, row=0)
    async def buttonLast(self, interaction:discord.Interaction, button: discord.ui.Button):
        self.symbol = "⏭"
        await self.turnPage(interaction=interaction,fast=1)
    #endDef

    @discord.ui.button(label="X", style=discord.ButtonStyle.red, row=0)
    async def buttonX(self, interaction:discord.Interaction, button: discord.ui.Button):
        await interaction.response.edit_message(
            content= self.options.get("end_message", f"Thanks {self.user}!"),
            embed = None,
            view = None
        )
    #endDef
#endClass
