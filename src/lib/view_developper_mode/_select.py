import discord

from ..db_guilds import DBGuilds

class _Select_Dev_Mode(discord.ui.Select):


    def __init__(self, interaction: discord.Interaction, **kwargs):
        activated = None
        with DBGuilds() as db:
            activated = int(db.get_datas(
                table_name=db.options["table_guilds"],
                conditions={
                    "id_guild":interaction.guild.id,
                },
                fields=["developper_mode",]
            )[0][0]) > 0
        #

        modact = 'activé' if activated else 'désactivé'

        self.keep_mod = {
            "label":"Conserver le mode actuel",
            "value":-1,
            "description":f"Mode développeur: {modact}",
            "default":True
        }

        self.dev_mod={
            "label":"Mode développeur",
            "value":1,
            "description":"Passer en mode développeur",
            "default":False
        }

        self.user_mod={
            "label":"Mode utilisateur",
            "value":0,
            "description":"Désactiver le mode développeur",
            "default":False
        }

        options={
            "placeholder":"Please enter a valid key first",
            "min_values":1,
            "max_values":1,
            "options":[
                discord.SelectOption(**self.keep_mod),
                discord.SelectOption(**self.dev_mod),
                discord.SelectOption(**self.user_mod),
            ]
        }

        options.update(kwargs)

        super().__init__(**options)

    #

    async def callback(self, interaction: discord.Interaction):
        await super().callback(interaction)

        mod_selected = self.update_mods(values=self.values[0])
        self.update_view()
        await interaction.response.edit_message(content=f"Selected: {mod_selected}", view=self.view)

        pass
    #

    def update_mods(self, values=None):

        if not values or len(values) == 0 or not values[0] or not values[0].isdigit():
            modselect = 'do nothing after update'
        elif int(values[0]) > 0:
            modselect = 'activation after update'
        elif int(values[0]) == 0:
            modselect = 'unactivation after update'
        else:
            raise ValueError(f"Values: {values}")
        #

        # Cette boucle ne fait rien : c'est normal. Je la garde, si jamais je voulais changer ça
        # for i in range(len(self.options)):
        #     if int(self.options[i].value)==-1:
        #         self.options[i].description=f"{self.options[i].description.split('--')[0]}"
        #     #
        # #

        return modselect
    #

    def update_view(self):

        for e in self.view.choices_list.options:
            e.default=False
        #

        if int(self.values[0]) == -1:
            self.view.choices_list.options[0].default=True
            self.view.choices_list.placeholder = self.keep_mod["label"]
        elif int(self.values[0]) == 1:
            self.view.choices_list.options[1].default=True
            self.view.choices_list.placeholder = self.dev_mod["label"]
        elif int(self.values[0]) == 0:
            self.view.choices_list.options[2].default=True
            self.view.choices_list.placeholder = self.user_mod["label"]
        #

#


