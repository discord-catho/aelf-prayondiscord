import os
import discord
from dktotoolkit import write_message

from ..db_guilds import DBGuilds

from ._buttons import _Button_Validate, _Button_Auth_Modal_Button
from ._select import _Select_Dev_Mode

class View_Developper_Mode_Activate(discord.ui.View):

    def __init__(self, interaction:discord.Interaction, **kwargs):
        self.interaction = interaction
        self.selected = None
        super().__init__(**kwargs)


        self.choices_list = _Select_Dev_Mode(
            disabled=True,
            custom_id="list_of_choices",
            interaction=interaction
        )

        self.validate_button = _Button_Validate(
            menu=self.choices_list,
            disabled=True,
            custom_id="validation_button"
        )

        self.cancel_button = discord.ui.Button(
            label='Annuler',
            style=discord.ButtonStyle.danger,
            custom_id="cancel_button"
        )
        write_message("Cancel button not implemented!", level="debug")

        self.open_auth_modal_button = _Button_Auth_Modal_Button(
            choices_list=self.choices_list,
            validate_button=self.validate_button,
        )

        # Ajout des composants à la vue
        self.add_item(self.cancel_button)
        self.add_item(self.open_auth_modal_button)
        self.add_item(self.choices_list)
        self.add_item(self.validate_button)
    #
#
