import discord
from dktotoolkit import write_message

from ..load_translations import load_translations
from ..db_guilds import DBGuilds
from ..modal_private_mode import Modal_Open_Private_Mode
from ._select import _Select_Dev_Mode

txt = load_translations("fr", nested_path="core/rsa_key")

class _Modal_Connect_Private_Mode(Modal_Open_Private_Mode):

    def __init__(self, view=None, activate_items=[], deactivate_items=[], **kwargs):
        self.activate_items = activate_items
        self.deactivate_items = deactivate_items
        self.view=view
        super().__init__(**kwargs)
    #

    async def on_submit(self, interaction: discord.Interaction):
        write_message("rationnaliser cette fonction", level="debug")
        try:
            valide_key = await super().on_submit(interaction=interaction, force_key=True)
        except ValueError:
            msg = txt["wrong_key"]+" (on_submission)"
            write_message(msg, level="warning")
            await interaction.response.edit_message(content=msg, view=self.view)
            return
        except Exception as e:
            print(e)
            raise
        #

        if valide_key:
            for elt in self.activate_items:
                elt.disabled = False
            #
            for elt in self.deactivate_items:
                elt.disabled = True
            #
        #
        await interaction.response.edit_message(content=txt["choose_option"], view=self.view)
    #

#


class _Button_Validate(discord.ui.Button):
    def __init__(self, menu, private_key=None, **kwargs):
        self.menu = menu
        self.private_key = private_key
        options={
            "style":discord.ButtonStyle.primary,
            "label":"Upload update",
        }

        options.update(kwargs)

        super().__init__(**options)
    #

    async def callback(self, interaction: discord.Interaction):
        if not self.menu.values:
            change = -1
        else:
            change = int(self.menu.values[0])
        #

        with DBGuilds() as db:
            if change >= 0:
                db.update_data(
                    table_name=db.options["table_guilds"],
                    data_dict={
                        "id_guild":interaction.guild.id,
                        "developper_mode":'0' if change == 0 else '1'
                    },
                    new_datas_col=["developper_mode",],
                    commit=True
                )
            #
            activated = int(db.get_datas(
                table_name=db.options["table_guilds"],
                conditions={
                    "id_guild":interaction.guild.id,
                },
                fields=["developper_mode",]
            )[0][0]) > 0
        #
        if change >= 0:
            await interaction.response.edit_message(content=f'Developper mode: {"activated" if activated else "deactivated"}', view=None)
        else:
            await interaction.response.edit_message(content=f'Developper mode non changé: {"activated" if activated else "deactivated"}', view=None)
        #
    #
#




class _Button_Auth_Modal_Button(discord.ui.Button):
    def __init__(
            self,
            choices_list,
            validate_button,
            **kwargs
    ):
        self.choices_list=choices_list
        self.validate_button=validate_button
        options={
            "style":discord.ButtonStyle.primary,
            "label":"Unlock the setting",
        }

        options.update(kwargs)
        super().__init__(**options)
    #

    async def callback(self, interaction: discord.Interaction):
        modal = _Modal_Connect_Private_Mode(
            activate_items=[self.choices_list, self.validate_button],
            deactivate_items=[self,],
            modal=None,
            view=self.view
        )
        authorized = False
        try:
            authorized = await interaction.response.send_modal(modal)
        except Exception as e:
            print(e)
            raise
        #
    #
#
