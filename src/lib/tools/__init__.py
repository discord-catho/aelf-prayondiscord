
def classtodict(obj):
    return dict((k, getattr(obj, k)) for k in dir(obj) if not k.startswith('_'))
