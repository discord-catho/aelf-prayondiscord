# translation_manager.py
import os
import json
import logging

def load_constants(filename):
    """
    charger le fichier ./src/assets/__constants.py
    """

    constants = {}
    with open(filename, 'r') as file:
        exec(file.read(), constants)
    return constants
#

def process_json_structure(data, constants, prefix="%"):
    '''
    Convertir les entrée du type %MA_VARIABLE en MA_VARIABLE et regarder la valeur
    de celle-ci dans le dico sontants.

    Attention, ne pas ecrire x = process_json_structure(x, c) : retourne None.
    Les donnees sont modifiees juste par l'appel a la fonction.

    :param Union[list, dict] data: JSON-like structure
    :param dict constants: les contantes
    :param str prefix: le préfix pour détecter des variables dans le fichier
    :return: None
    '''
    if isinstance(data, dict):

        for key, value in data.items():
            if isinstance(value, str) and value.startswith(prefix):
                var_name = value[len(prefix):]
                if var_name in constants:
                    data[key] = constants[var_name]
                else:
                    logging.warning(f"Value '{value}' in key '{key}' not found in constants.py")
                #
            elif isinstance(value, (dict, list)):
                process_json_structure(value, constants)
            #
        #

    elif isinstance(data, list):

        for i, item in enumerate(data):
            if isinstance(item, str) and item.startswith(prefix):
                var_name = item[len(prefix):]
                if var_name in constants:
                    data[i] = constants[var_name]
                else:
                    logging.warning(f"Value '{item}' at index {i} not found in constants.py")
                #
            elif isinstance(item, (dict, list)):
                process_json_structure(item, constants)
            #
        #
    #
#


def load_translations(
        language:str,
        path:str="lang",
        ext:str="json",
        default_key:str="name",
        nested_path="./",
        sep = "/",
        filename_constants='./src/assets/__constants.py'
)->dict:
    """Load translations (from jsons) for lang directory

    :param str langague: The language
                        (using the internationnal abbreviation, for exemple "fr" for French)
    :param str path: The path (default: 'lang' for './lang')
    :param str ext: The format (defaut, json). Only json implemented yet (yaml will be come later)
    :param str default_key: if the data is str, use a default key to return a dict ; if None, return the value
    :param str nested_path:   Path to access nested data within the JSON-like structure.

    Usage
    =====

    * In source :

        from utils import load_translations
        _tr = load_translations("fr")  # Load the lang of the package
        _tr.update(load_translations("fr", path="subdir/lang"))  # Update with the submodule

    * File lang/xx.json (with content)

        {
         'word':'mon mot',
         'foo':'base'
        }

    * File lang/xx.json (**without** content : do **not** have empty file)

        {}

    """
    if not isinstance(ext, str):
        raise ValueError(f"ext={ext}, type {type(ext)} not a string")
    #
    if ext.lower()=="json":
        with open(os.path.join(path, f'{language}.json'), 'r') as file:
            content = json.load(file)
        #
    else:
        raise ValueError("Only ext='json' implemented yet (file: {path} ; ext: {ext})")
    #


    if len(nested_path) > 1 and nested_path[0] == ".":
        nested_path = nested_path[1:]
    elif len(nested_path) <= 1:
        nested_path = ''
    #
    if nested_path[0] == "/":
        nested_path = nested_path[1:]
    elif len(nested_path) <= 1:
        nested_path = ''
    #

    current_data = content
    path_parts = nested_path.split(sep)

    previous_part=""
    for part in path_parts:
        previous_part += sep+part
        current_data = current_data.get(part)

        if current_data is None:
            raise ValueError(f"{previous_part} is not founded in the file: {path}")
        #
    #

    # Transformer les variables
    constants_data = load_constants(filename_constants)
    process_json_structure(current_data, constants_data)
    #

    if default_key is None:
        return current_data
    else:
        return {default_key:current_data} if isinstance(content, str) else current_data
    #

    raise ValueError("default_key: {default_key}")
#
