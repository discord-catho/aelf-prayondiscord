import discord
from dktotoolkit import write_message

from ..db_guilds import DBGuilds
from ..load_translations import load_translations

from ._permissions_priority import permissions_priority

def check_permissions_database(command:str="", command_category:str=None, autorize_admins:bool=True):
    def predicate(interaction: discord.Interaction) -> bool:

        user_id = interaction.user.id
        roles_id = [e.id for e in interaction.user.roles]
        roles_id.append(None)
        channel_id = interaction.channel_id
        guild_id = interaction.guild_id

        authorized=False

        with DBGuilds() as db:
            all_authorized = []
            L = []
            for each_role_id in roles_id:
                #for each_user_id in user_id:
                #    for each_channel_id in channel_id:
                all_authorized.append(
                    db.check_authorizations(
                        command=command,
                        command_category=command_category,
                        guild_id=guild_id,
                        channel_id=channel_id,
                        user_id=user_id,
                        role_id=each_role_id
                    )
                )
                L.extend(all_authorized)
            #
        #

        # Mettre la plus haute autorisation en première position
        all_authorized = permissions_priority(L)

        authorized = all_authorized[0].get("allowed", False)

        if (not authorized) and autorize_admins and interaction.user.guild_permissions.administrator :
            try:
                mymessage=load_translations("fr",nested_path="decorators")
                messages={"admin_override":"L'admin fait ce qu'il veut, et il y a un bug ici..."}
            except Exception as e:
                print(e)
                raise
            #
            print(messages["admin_override"])
            write_message(messages["admin_override"], level="info")
            authorized = True
        #

        return authorized

    # endDef Predicate

    return discord.app_commands.check(predicate)
#
