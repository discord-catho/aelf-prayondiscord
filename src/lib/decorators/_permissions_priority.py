from dktotoolkit.list import aplatir_liste

def permissions_priority(permissions):
    """
    permissions = [{id_command, guild_id, channel_id, user_id, role_id, command_name, allowed}, ...]
    Ici, plus la valeur est faible, mieux c'est. Dans 'priority_order', les plus importants sont en premiers.
    La liste est finalement triée pour que l'élément avec le plus fort résultat soit le plus haut.
    """

    priority_order = ['command_name', 'command_group', 'user_id', 'channel_id', 'role_id']

    len_priority_order = len(priority_order)

    def get_priority(item, priority_order_index):
        return 0  if item is None else 2  ** (len_priority_order-priority_order_index)
    #

    def calculate_priority(permissions):
        priority = 0
        for i in range(len(priority_order)):
            priority += get_priority(permissions.get(priority_order[i]), i)
        #
        permissions["priority"] = priority
    #

    def sort_by_priority(data, reverse=False):
        '''
        True: plus haute valeur en 1er
        '''
        return sorted(data, key=lambda x: x['priority'], reverse=reverse)
    #

    permissions = aplatir_liste(permissions)

    for i in range(len(permissions)):
        calculate_priority(permissions[i])
    #

    sorted_permissions = sort_by_priority(permissions, reverse=True)

    return sorted_permissions
#
