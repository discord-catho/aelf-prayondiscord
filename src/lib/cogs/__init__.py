import logging

from discord.ext import commands

from ..db_guilds import DBGuilds
from ..tools import classtodict

def available_guilds():
    with DBGuilds() as db:
        available_guilds = db.get_guilds()
    #
    return available_guilds
#


class CogsParent(commands.Cog):

    options = {
        "guilds":[],
        "roles":[]
    }

    class DefaultOptions:
        guilds = []
        roles = []
        verbose = False

    class Options:
        pass

    def __init__(self, client, **options):

        self.options.update(classtodict(self.DefaultOptions))
        self.options.update(classtodict(self.Options))
        self.options.update(options)

        self.client = client

        self.logger = logging.getLogger(__name__)
    #endDef

#endClass
