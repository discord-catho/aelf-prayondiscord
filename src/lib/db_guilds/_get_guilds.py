def get_guilds(self, only_authorized:bool=True, raw:bool=False):
    """
    Récupère les informations sur les guildes depuis la base de données.

    :param bool,optional only_authorized: Indique s'il faut uniquement retourner les guildes autorisées. Par défaut True.
    :param bool,optional raw: Indique s'il faut retourner les données brutes ou seulement les IDs des guildes. Par défaut False.

    list: Liste des IDs des guildes ou les données brutes, selon la valeur de `raw` et 'only_authorized'.
    """

    keys = ["id_guild", "authorized_guild"]
    table = self.table_guilds
    keys_table=[f"{table}.{e}" for e in keys]

    query=f"""
SELECT {",".join(keys_table)}
FROM {table}
ORDER BY {table}.id_guild
"""

    datas_out = self.request_db(query)

    if raw:

        return datas_out

    else:

        # Traitement des donnees
        datas = [(int(e[0]), (str(e[1]).lower() in ["1", "true", "t", "yes", "y", "oui", "o"])) for e in datas_out]

        if only_authorized:
            datas = [(e[0], e[1]) for e in datas if e[1]]
        #
        return [e[0] for e in datas]
    #
