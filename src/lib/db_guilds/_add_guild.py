def add_guild(self,
              id_guild,
              guild_name:str="installed from environnement var",
              installer_id:int=None,
              authorized_guild:bool=False,
              restrict_members_usage:bool=True,
              update:bool=False
            )->None:
    """
    Ajoute ou met à jour une entrée dans la table 'guild' de la base de données.

    :param id_guild: L'identifiant unique de la guilde.
    :type id_guild: int
    :param guild_name: Le nom de la guilde (par défaut, 'installed from environment variable').
    :type guild_name: str
    :param installer_id: L'identifiant de l'installateur de la guilde (par défaut, None).
    :type installer_id: int
    :param authorized_guild: Indique si la guilde est autorisée (par défaut, False).
    :type authorized_guild: bool
    :param update: Indique s'il faut mettre à jour les entrées existantes (par défaut, False).
    :type update: bool
    :param bool restrict_members_usage: Restreindre ou non l'utilisation par les membres des commandes par defaut

    :return: None
    :rtype: None
    """

    self.insert_data(
        table_name=self.options["table_guilds"],
        data_dict={
            "id_guild":id_guild,
            "guild_name":guild_name,
            "installer_id":installer_id,
            "authorized_guild":authorized_guild,
        },
        allow_duplicates=False,
        replace_on_datas=["id_guild", "installer_id"],
        commit=True
    )

    self.insert_data(
        table_name=self.options["table_permissions_commands"],
        data_dict={
            "guild_id":id_guild,
            "allowed":restrict_members_usage,
            "command_name":None,
            "channel_id":None,
            "user_id":None,
            "role_id":None
        },
        replace_on_datas=["guild_id", "command_name", "channel_id", "user_id", "role_id"],
        allow_duplicates=False,
        commit=True
    )

    self.insert_data(
        table_name=self.options["table_aelf_guilds"],
        data_dict={
        "id_guild":id_guild,
        },
        allow_duplicates=False,
        commit=True
    )

    # Par defaut, on ajoutera ces elements
    kwargs_assets={
        "replace_on_datas":["guild_id", "key_name"],
        "skip_to_update":["filename",],
        "allow_duplicates":False,
        "commit":True
        }

    self.insert_data(
        table_name=self.options["table_aelf_assets_elements"],
        data_dict={
            "guild_id":id_guild,
            "key_name":"doxologie",
            "filename":"doxologie_short"
        },
        **kwargs_assets
    )

    self.insert_data(
        table_name=self.options["table_aelf_assets_elements"],
        data_dict={
            "guild_id":id_guild,
            "key_name":"pater_noster",
            "filename":"pater_noster_short"
        },
        **kwargs_assets
    )
    return
