import os
from threading import Lock # verrou

from ..tools import classtodict

from dktoeasysqlite3 import MyDB


class DBGuilds(MyDB):

    class DefaultOptions:
        # SEE: ./database/create_tables.sql and others
        path="./datas/bot_aelf.db"
        path="./datas/bot.db"

        table_guilds="guilds"
        table_permissions_commands="permissions_commands"

        prefix="BOT"

        to_html=True
        verbose=False

        delete_if_error=False
    #

    class Options:
        table_aelf_guilds="aelf_guilds"
        table_aelf_assets_elements="aelf_assets_elements"
    #

    def __init__(self, **options):
        self.options = classtodict(DBGuilds.DefaultOptions)
        self.options.update(classtodict(DBGuilds.Options))
        self.options.update(options)

        self.db_path = self.check_environ(
            name="path",
            prefix=f"{self.options.get('prefix')}_db"
        )

        try:
            super().__init__(
                createIfNotExists="./sql_scripts/create_database.sql",
                lock_in=Lock()
            )
        except Exception as e:
            if delete_if_error:
                write_message(f"File {file} removed because error during the creation", level="critical")
                file=self.path
                os.remove(file)
            else:
                write_message(f"File {file} not removed but error during the creation", level="critical")
            #
            raise
        #

        prefix_table=f"{self.options.get('prefix')}"
        self.table_guilds=self.check_environ(
            name="table_guilds",
            prefix=prefix_table
        )
        self.options["table_guilds"] = self.table_guilds

        self.table_permissions_commands=self.check_environ(
            name="table_permissions_commands",
            prefix=prefix_table
        )
        self.options["table_permissions_commands"] = self.table_permissions_commands

        self.table_aelf_guilds=self.check_environ(
            name="table_aelf_guilds",
            prefix=prefix_table
        )
        self.options["table_aelf_guilds"] = self.table_aelf_guilds
    #endDef

    from ._get_guilds import get_guilds
    from ._add_guild import add_guild
    from ._permissions_command import check_authorizations

    def check_environ(self, name, prefix=None):
        name_table = self.options.get(name)
        environ_table = f"{prefix}_{name}"
        return os.environ.get(environ_table, name_table)
    #

#endClass

