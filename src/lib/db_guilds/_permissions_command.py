def check_authorizations(self, command: str, guild_id: int, channel_id: int, command_category: str = None, user_id: int = None, role_id: int = None):
    """
    Vérifie les autorisations d'exécution d'une commande.

    :param str command: Le nom de la commande Discord.
    :param str command_category: La catégorie de la commande Discord (optionnelle).
    :param int guild_id: L'identifiant du serveur.
    :param int user_id: L'identifiant de l'utilisateur (optionnel).
    :param int role_id: L'identifiant du groupe (optionnel).

    :return: Une liste des identifiants autorisés.
    :rtype: list[int]

    :raises ValueError: Si les arguments guild_id et (user_id ou group_id) ne sont pas spécifiés.

    :raises ValueError: Si l'utilisateur n'est pas autorisé à exécuter la commande.

    Exemple d'utilisation::

        authorized_ids = check_authorizations('ma_commande', guild_id=12345, user_id=67890)

    """

    # Vérification des arguments
    if not (guild_id): # and (user_id or role_id)):
        raise ValueError("L'argument guild_id est obligatoire")
    #

    print("R>", guild_id, role_id, user_id, command, channel_id)
    Lout = ["id_command", "allowed", "channel_id", "role_id", "user_id", "command_name"]
    query = f"SELECT {','.join([e for e in Lout])} FROM permissions_commands WHERE guild_id = ?"

    datas = [guild_id,]
    query_order = []


    # role_id
    query += " AND (role_id = ? OR role_id = 0 OR role_id = -1 OR role_id IS NULL) "
    datas.append(0 if role_id is None else role_id)
    query_order += ["role_id DESC",]
    #

    # user_id
    datas.append(0 if user_id is None else user_id)
    query += " AND (user_id = ? OR user_id = 0 OR user_id = -1 OR user_id IS NULL) "
    query_order += ["user_id DESC",]
    #

    # command
    if command_category:
        query += " AND (command_name = ? OR command_name = ? OR command_name is NULL) "
        query_order += ["command_name DESC",]
        datas.extend([command, command_category])
    elif command is not None and command:
        query += " AND (command_name = ?  OR  command_name is NULL)"
        query_order += ["command_name DESC",]
        datas.append(command)
    elif command == "":
        pass
    else:
        raise ValueError("Command must be empty or has value, but not None")
    #

    # channel_id
    datas.append(0 if channel_id is None else channel_id)
    query += " AND (channel_id = ? OR channel_id = 0 OR channel_id = -1 OR channel_id IS NULL)"
    query_order += ["channel_id DESC",]
    #

    query = f"{query} ORDER BY {', '.join(query_order)}"

    # print("permissions_commands>", query, datas)
    # Maintenant, exécutez la requête et obtenez la liste des identifiants autorisés
    cursor = self.request_db(query, datas)

    authorized_ids = [{Lout[i]:e[i] for i in range(len(Lout))} for e in cursor]
    return authorized_ids
