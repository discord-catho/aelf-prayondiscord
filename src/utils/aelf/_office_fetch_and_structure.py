import sys
import json
import re

if __name__=="__main__":
    import os
    sys.path.insert(0, os.path.abspath('../..'))
#end

from dktotoolkit import compat_mode, write_message

from ..tools import insert_doxologie

def fetch_and_structure_office_data(self, hunfolding=[], **kwargs):
    """
    :param list hunfolding: Hunfolding (usefull to repeat the "antienne" for exemple)
    :param **kwargs: Divers arguments
                    - verbose
                    - set_doxologie={"texte":"Gloire au Père...", "author":None, ...} pour remplacer la doxologie par exemple (mais général)
    :return: hunfolding with datas from AELF
    :rtypes: list
    """

    # Get datas from AELF API
    try:
        datas_api = self.call_api_aelf(
            office_name=self.office,
            date=self.date,
            zone=self.zone
        )
    except Exception as e:
        write_message(f"Error : {e}"+"\n", level="critical", verbose=kwargs.get("verbose", False))
        raise e
    #

    try:
        datas_api[self.office] = self.office_content_to_elements(datas_api[self.office], hunfolding=hunfolding)
    except:
        write_message("UNEXPECTED !\n", level="critical", verbose=kwargs.get("verbose", False))
        raise
    #

    if not "informations" in datas_api or not datas_api["informations"]:
        raise ValueError(f"Unexpected empy 'datas_api['informations']' : please check {self.zone}, {self.date}, {self.office}:  {datas_api['informations']}")
    elif not self.office in datas_api or not datas_api[self.office]:
        raise ValueError(f"Unexpected empy 'datas_api[self.office]' : please check {self.zone}, {self.date}, {self.office}:  {datas_api[self.office]}")
    #

    if isinstance(datas_api["informations"], list) or isinstance(datas_api["informations"], tuple) and len(datas_api["informations"]) == 1:
        datas_api["informations"] = datas_api["informations"][0]
    elif isinstance(datas_api["informations"], list) or isinstance(datas_api["informations"], tuple):
        raise ValueError(f"Unexpected empy 'datas_api['informations']' (len = {len(infos)}) : please check {self.zone}, {self.date}, {self.office} :  {datas_api['informations']}")
    #endIf

    if hunfolding:
        datas_api[self.office] = insert_doxologie(datas_api[self.office])
    #

    for elt in kwargs.keys():
        if len(elt) < 4 or not "set_" in elt[:4]:
            continue
        #
        for i in range(len(datas_api[self.office])):
            if elt[4:] == datas_api[self.office][i]["key_name"]:
                datas_api[self.office][i].update(kwargs[elt])
            #
        #
    #
    self.datas[self.office] = datas_api[self.office]
    self.datas["informations"].update(datas_api["informations"])
#
