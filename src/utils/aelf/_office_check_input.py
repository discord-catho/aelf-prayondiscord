import logging
import sys

def check_input(self):

    """
    Vérifie les paramètres d'entrée de l'objet OfficeAELF.

    :returns: Un dictionnaire contenant le statut de vérification.
        - Si les paramètres sont valides, le dictionnaire contient {"status": "200"}.
        - Si des erreurs sont détectées, le dictionnaire contient {"status": "404", "error": "<liste_des_erreurs>"}.
    :rtype: dict
    """

    err = []
    if self.options["format_output"] is None:
        logging.warning("format_output is not setted : use 'simple'")
        self.options["format_output"]="simple"
    elif self.options.get("format_output", "").lower() not in self.options["available_format_output"]:
        msg = f"OfficeAELF.__init__() : formatting = {self.options['format_output']}, not in {self.options['available_format_output']} ! I'll use '{self.options['available_format_output'][0]}'"
        logging.warning(msg)
        err += [msg,]
        self.options["format_output"] = self.options['available_format_output'][0]
    #endIf

    if self.zone is None:
        logging.warning("zone is not setted")
    elif self.zone.lower() not in self.options["available_zone"]:
        msg=f"OfficeAELF.__init__() : zone = {self.zone}, not in {self.options['available_zone']} !  I'll use '{self.options['available_zone'][0]}'"
        logging.warning(msg)
        err += [msg,]
        self.zone = self.options['available_zone'][0]
    #endIf

    if self.office is None or not isinstance(self.office, str):
        msg=f"OfficeAELF.__init__() : office = {self.office}, not in {self.options['available_office']} ! STOP 1"
        logging.error(msg)
        err += [msg,]
        self.office = None
    elif self.office.lower() not in self.options["available_office"]:
        msg=f"OfficeAELF.__init__() : office = {self.office}, not in {self.options['available_office']} ! STOP 2"
        logging.error(msg)
        err += [msg,]
        #self.office = None
    #endIf

    if err:
        sys.stderr.write(">>"+"\n>>".join(err))
        return {"status" : 404, "error" : "\n".join(err)}
    else:
        return {"status":200}
    #endIf

    raise Exception
#endDef
