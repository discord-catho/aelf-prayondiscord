import os
import sys
import json

from dktotoolkit import parser_date, compat_mode, write_message


def _todict(obj):
    # https://github.com/matthewwithanm/python-markdownify/blob/develop/markdownify/__init__.py
    return dict((k, getattr(obj, k)) for k in dir(obj) if not str(k).startswith('_'))
#

# For statics
from ._call_api import _call_api_aelf
from ._office_content_to_elements import _office_content_to_elements

class AELF(object):
    """
RECUPERER LES INFOS DU BOT (office)

:param list(str) available_source: = ["aelf", ]
:param list(str) available_format_output: = ["html", "markdown"]
:param list(str) available_zone: = ["francais", "romain"]
:param list(str) available_office: = ["vepres", "complies"] #TODO : a remplir !!

:param str format_output: Format de sortie
:param str date: date (YYYY-MM-DD) Date de l'office
:param str zone: Calendrier / zone utilisé
:param str office: nom de l'office
:param str source: source principale de l'office (uniquement AELF actuellement)
"""
    class DefaultOptions:
        available_format_output = ["discord", "no_formatting"]

        available_zone = ["romain", "france", "luxembourg", "belgique"]

        available_office = ["laudes", "tierce", "sexte", "none", "vepres", "complies", "lectures", "messe"]

        date="today"
        zone="romain"

        details=False

        #
        format_output="discord"
        discord_wrap=True
        discordwrap_width=1020
        discordwrap_keeplines=False

    #

    class Options(DefaultOptions):
        zone="france"
        pass
    #

    def __init__(self, **kwargs):
        """
Constructeur

:param str formatting: [html, markdown] format de sortie
:param str zone: calendrier
:param str|Date date: date de l'office
:param str office: nom de l'office
:param str source: source (AELF uniquement actuellement)
"""
        # Create an options dictionary. Use DefaultOptions as a base so that
        # it doesn't have to be extended.
        # https://github.com/matthewwithanm/python-markdownify/blob/develop/markdownify/__init__.py

        kwargs = compat_mode("date", ["jour", "day"], replace_in_kwargs=True, **kwargs)
        kwargs = compat_mode("office", ["name", "nom"], replace_in_kwargs=True, **kwargs)
        kwargs = compat_mode("zone", ["calendrier", "calendar" ], replace_in_kwargs=True, **kwargs)

        self.options = _todict(AELF.DefaultOptions)
        self.options.update(_todict(AELF.Options))
        self.options.update(kwargs)

        if self.options.get("date", None) is None:
            write_message("Warning, date is None", level="warning")
        else:
            self.date = parser_date(self.options.get("date", None).lower())  # Appeler la fonction "convert_date" ou jsp comment : YYYY-MM-DD
        #
        self.zone = self.options.get("zone", None).lower()
        self.office = self.options.get("office", None).lower()

        self.datas = {
            "informations": {"source":"AELF - Association Épiscopale pour la liturgie française"},
            self.office if self.office else "any_office":{},
        }


    #endDef

    # Functions for office :
    from ._office_check_input import check_input
    from ._office_fetch_and_structure import fetch_and_structure_office_data

    # Other functions
    from ._parse_datas import parse_datas

    # Assessors :
    from ._get_office import get_office

    def update_attributes(self, format_output:str=None,**kwargs):
        """
        Update attributes of the class from the kwargs
        """
        if not options.get("format_output", False):
            pass
        elif not isinstance(options.get("format_output"), str):
            sys.stderr.write(f"> Office.get_office(): format_output={options.get('format_output')} must be a string\n")
        elif format_output.lower() in self.available_format_output:
            self.format_output = format_output.lower()
        elif format_output.lower() not in self.available_format_output:
            sys.stderr.write(f"> Office.get_office(): format_output={options.get('format_output')} not available ; please use one in {self.available_format_output}\n")
        else:
            sys.stderr.write(f"> Office.get_office(): format_output={options.get('format_output')} unexpected\n")
        #endIf

        return kwargs
    #endDef

    def update_db(self):
        self.update_hunfolding()
    #endDef


    # Static
    @staticmethod
    def call_api_aelf(
            office_name,
            date,
            zone=None,
            return_alldatas=True # retourner toutes les donnees ou juste la priere
    ):
        """
        TODO
        ====
        Homogeneiser calendar / zone

        Description
        ===========

        Recuperer le dictionnaire de donnees d'aelf (vient de ProphetiS)

        :param str office_name: nom de l'office
        :param str date: jour
        :param str zone: calendrier utilise
        :param bool return alldatas: Retourner toutes les donnees (informations + priere) ou juste la priere
        """

        return _call_api_aelf(
            office_name=office_name,
            date=date,
            zone=zone,
            return_alldatas=return_alldatas
        )
    #

    @staticmethod
    def office_content_to_elements(
            office_content,
            hunfolding:list=[],
            skip_empty_items:bool=True
    ):
        """
        :param dict office_content: datas from AELF API, v1
        :param list hunfolding: Hunfolding (usefull to repeat the "antienne" for exemple, optionnal)
        :param bool skip_empty_item: skip empty items, or keep it inside the hunfolding ?
        :return: hunfolding (or "hunfolding-like") with datas from AELF
        :rtypes: list
        """

        return _office_content_to_elements(
            office_content=office_content,
            hunfolding=hunfolding,
            skip_empty_items=skip_empty_items
        )
    #

#endClass
