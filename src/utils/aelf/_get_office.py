import sys
import json
import re
import bs4

if __name__=="__main__":
    import os
    sys.path.insert(0, os.path.abspath('../..'))
#end

from dktotoolkit import ParserHTML, clean_json
from dktotoolkit.dict import unprefix_keys
from dktotoolkit import discordify_dict



KEY_NAME="key_name" # ex :cle_element

def get_office(self, **kwargs)->None:
    """Recuperer tout l'office a partir de la base de donnee, retourne les donnees au format self.options['format_output']

    :param str kwargs[format_X]: output format (format_discordwrap, format_discordwrap_width)
    :param str kwargs[office_Y]: office kwargs (office_doxologie, ...)
    :returns: office
    :rtypes: dict
    """

    status = self.check_input()
    if status.get("status", -1) not in [200, 202]:

        self.data.update(status)

        return status.get("status", -1)
    #

    try:
        self.fetch_and_structure_office_data(**kwargs)
    except :
        raise
    #

    # 4. Si formatting = html : conversion recursive
    # 4. Si formatting = markdown : conversion recursive

    self.parse_datas(**kwargs)  # Conversion au bon format
    self.datas.update(status)  # Ajout du code 200 ou 202
    return self.datas
#endDef
