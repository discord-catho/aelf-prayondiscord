_this_file_="cogs.offices._generate_office.py"

from discord import Embed
import sys
import os
import json
from dktotoolkit.html import request_html_page

from dktotoolkit import write_message

from assets.__constants import AELF_ASSETS_DEFAULT, AELF_ASSETS_SET, AELF_ASSETS_UNSET, AELF_ASSETS_HUNFOLDING_RELATIVE_API, AELF_ASSETS_HUNDOLDING_RELATIVE_FILE, AELF_ASSETS_CONTENT_RELATIVE_API, AELF_ASSETS_CONTENT_RELATIVE_FILE

from ..aelf import AELF
from ..tools import colorname2code
from lib import create_embed, publishEmbeds
from lib import DBGuilds
from requests.exceptions import ConnectionError

from ._content_to_field import _content_to_fields

"""
@brief: Generer les offices
"""


def _assets_dev_mode(assets_path, relative_path, sep="/"):
    '''
    Mode développeur : utilisation d'une API
    Afin de pouvoir tester des corrections sans redémarrage du bot

    :param str relative_path: Chemin une fois connecte a assets_path
    '''

    try:
        write_message("supprimer la reference AELF de hunfolding", level="debug")
        content = request_html_page(
            url=f"{assets_path}{sep}{relative_path}",
            format_output="json"
        ).get("content", [])
    except ConnectionError:
            write_message(f"Error to connect {assets_path}", level="error")
            assets_path = AELF_ASSETS_DEFAULT
            content = {}
            pass
    #
    return content, assets_path
#


def _capitalize_first_word(line, default_return=None):
    """
    Capitalize first word, but keep also other cap letters
    """

    if len(line) <= 0:
        return default_return
    #

    ls = line.split()
    word1 = ls[0].capitalize()
    return " ".join([word1,]+ls[1:]) if len(ls) > 1 else word1
#


async def generate_office(self, interaction, name:str, date:str="today", zone:str=None, **kwargs)->None:

    if name is None:
        return ValueError("name is none")
    #

    if zone is None:
        with DBGuilds() as db:
            zone = db.get_datas(
                table_name=db.options["table_aelf_guilds"],
                conditions={"id_guild":interaction.guild.id},
                fields=["zone",]
            )[0][0]
        #
    #

    # Aller chercher le deroule ici, ou non

    # Office(**office_parameters)
    office_parameters = {
        'office':name,
        'date':date,
        'verbose':kwargs.get("verbose", False)
    }

    # Office().get_office(**office_args)
    office_args = {
        'discordwrap_width':self.options["discordwrap_width"]
    }  # Je crois que c'est ici que je demande le deroule.


    with DBGuilds() as db:
        # renommer use_hunfolding en assets_path !!!
        # = f'./src/assets'
        assets_path=db.get_datas(
            table_name=db.options["table_aelf_guilds"],
            fields=["use_hunfolding",],
            conditions={"id_guild":interaction.guild.id},
        )[0][0]
    #

    hunfolding = None

    if not isinstance(assets_path, str):
        raise ValueError(f"assets_path={assets_path} not a string!")
    #

    if assets_path[:4].lower() == "http":
        # Mode développeur, afin de pouvoir tester des corrections sans redémarrage du bot
        hunfolding, assets_path = _assets_dev_mode(
            assets_path=os.path.join(
                assets_path,
                AELF_ASSETS_HUNFOLDING_RELATIVE_API
            ),
            relative_path=name
        )
    #

    if assets_path == AELF_ASSETS_UNSET or not assets_path:

        hunfolding = {}

    elif assets_path == AELF_ASSETS_SET:

        #hundolding = read({name}.json)
        path = os.path.join(
            assets_path,
            AELF_ASSETS_HUNDOLDING_RELATIVE_FILE,
            f'{name}.json'
        )
        if not os.path.exists(path):
            raise FileNotFoundError(f"File not exists: {path}")
        #
        with open(path, 'r') as file:
            hunfolding = json.load(file)
        #
    elif hunfolding is None or not hunfolding:
        # Si on a un déroulement, c'est que l'on est en mode dev
        msg = f"Unexpected: assets_path = {assets_path} must be in "
        msg += f"['{AELF_ASSETS_SET}', '{AELF_ASSETS_UNSET}'] ; hunfolding = {hunfolding}"
        raise ValueError(msg)

    #

    with DBGuilds() as db:
        assets_keys = [{"key":e[0],"filename":e[1]} for e in db.get_datas(
            table_name=db.options["table_aelf_assets_elements"],
            fields=["key_name", "filename"],
            conditions={"guild_id":interaction.guild.id},
        ) if e[0] and e[1]]
    #

    # Update
    elements={}
    for elt in assets_keys:
        key_name = elt["key"]
        filename = elt["filename"]

        tmp = ""

        if assets_path[:4].lower() == "http":
            tmp, assets_path = _assets_dev_mode(
                assets_path=os.path.join(
                    assets_path, AELF_ASSETS_CONTENT_RELATIVE_API
                ),
                relative_path=f"{filename}",
                sep=""
            )
        #

        if not tmp:
            path = os.path.join(
                AELF_ASSETS_DEFAULT,
                AELF_ASSETS_CONTENT_RELATIVE_FILE,
                f"{filename}.json"
            )
            with open(path, 'r') as file:
                tmp = json.load(file)
            #
        #

        elements[f"set_{key_name}"] = tmp
    #

    try:
        office = AELF(**office_parameters).get_office(hunfolding=hunfolding, **elements, **office_args)
    except Exception as e:
        write_message("Error (b)", level="critical")
        raise(Exception)
    #endTry

    if isinstance(office, dict):
        pass
    elif isinstance(office, list) and len(office) == 1:
        office = office[0]
    elif isinstance(office, list):
        write_message("content of var 'office':",level="critical")
        for e in office:
            write_message(e, level="critical")
        #
        raise ValueError
    else:
        write_message(f"office {name}:{office}", level="critical")
        raise ValueError
    #

    try:
        color = colorname2code(office["informations"].get("couleur", 0))
    except Exception as e:
        write_message(f"!>ERROR creatEmbedTemplate {_this_file_} (2)", level="critical")
        sys.stderr.write(str(e)+"\n")
        raise
    #


    #embed_template = discord_Embed(color = color,timestamp=None)

    try:
        jour = office.get("informations",{}).get('jour_liturgique_nom', None)
    except:
        write_message(f"Error with 'jour' (1) : {office.get('informations')}", level="critical")
        raise
    #

    if jour is None:
        try:
            jour = office.get("informations",{}).get('jour', '')
        except:
            write_message(f"Error with 'jour' (2) : {office.get('informations')}", level="critical")
            raise
        #
    #

    try:
        semaine = office.get("informations",{}).get('semaine', '')
    except:
        write_message(f"Error with 'semaine' : {office.get('informations')}", level="critical")
        raise
    #
    try:
        url = office.get("informations",{}).get('url', '')
    except:
        write_message(f"Error with 'url' : {office.get('informations')}", level="critical")
        raise
    #

    try:
        fields = _content_to_fields(office.get(name, []))
    except:
        write_message(f"office : {str(name)} :: {office.get(name, None)}", level="critical")
        raise
    #

    if jour is None:
        write_message(f"jour is None : {office.get('informations')}", level="critical")
    #

    description=f"**{name.title()}**"


    ligne1 = _capitalize_first_word(office.get("informations",{}).get("ligne1", ''))
    ligne2 = _capitalize_first_word(office.get("informations",{}).get("ligne2", ''))
    ligne3 = _capitalize_first_word(office.get("informations",{}).get("ligne3", ''))

    if ligne1:
        description+="\n*"+ligne1+"*"
    #
    if ligne2:
        description+="\n*"+ligne2+"*"
    #
    if ligne3:
        description+="\n*"+ligne3+"*"
    #
    description+="\n"+f"*Calendrier {zone.capitalize()}*"
    description+="\n"+56*"\_" # 66 pour PC, mais smartphone, seulement 56
    footer=60*"_" # 74 pour PC, 60 pour telephone
    footer+="""
(c) AELF (https://aelf.org) - tous droits réservés pour le contenu.
Retrouvez les offices ainsi que la traduction liturgique de la Bible sur le site https://aelf.org mais n'hésitez pas à aller en librairie (chrétienne) pour acheter un bréviaire ou la Bible.

Participez au développement du bot : https://framagit.org/discord-catho/aelf-prayondiscord
"""
    url_logo="https://raw.githubusercontent.com/HackMyChurch/aelf-dailyreadings/master/app/src/main/ic_launcher-web.png"
    write_message("TODO : gestion des droits de l'image !", level="debug")

    try:
        embed = create_embed(
            fields=fields,
            description=description,
            color=color,
            author_name="AELF",
            author_url=url, #"https://aelf.org",
            author_icon_url=url_logo,
            footer_text=footer,
            footer_icon_url=url_logo,
            thumbmail_url="https://cdn.discordapp.com/avatars/1002871164311445574/f8548cea8e7be75f0cbcd30c2e02d1e9.png?size=1024",
            max_field_length=self.options["max_field_length"],
            max_nb_fields=self.options["max_nb_fields"],
        )


    except:
        write_message(f"create_embed", level="critical")
        raise
    #endTry

    try:

        kwargs_sm = {
            "view":None,
            "embed":None,
            "ephemeral":True,
        }

        end_message="Merci d'avoir prié avec AELF.\nContinuez la prière en méditant la Bible (achetée chez votre libraire ou sur https://aelf.org/bible) :innocent:\nRecevez quotidiennement les textes du jour en vous abonnant à la newsletter sur https://www.aelf.org/abonnement."
        if isinstance(embed,list):
            kwargs_sm["view"] = publishEmbeds(
                #interaction=interaction,
                embedPages=embed,
                timeout=self.options["timeout"],
                end_message=end_message
            )
            kwargs_sm["embed"] = embed[0]
            kwargs_sm["delete_after"]=self.options["timeout"]+self.options["timeout_beforedelete"]
        elif isinstance(embed, Embed):

            kwargs_sm["embed"] = embed
        else:
            write_message(f"dict kwargs_sm", level="critical")
            raise ValueError
        #

        await interaction.response.send_message(f"{name}: {date} ", **kwargs_sm)
    except Exception as e:
        write_message(f"{kwargs_sm}", level="critical")
        raise(Exception)
    #endTry

#endDef
