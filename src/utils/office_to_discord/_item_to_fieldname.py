from dktotoolkit import write_message
from utils.tools import parseName

def _item_to_fieldname(elt):

    name = elt.get("name", "")
    if name and elt.get('titre', ''):
        name = f"{name} - {elt.get('titre', '')}"
    elif elt.get('titre', ''):
        name = elt.get("titre", "")
    #

    if not name and "introduction" in elt.get("element_key", ""):
        name = "Introduction"
    elif not name and "hymne" in elt.get("element_key", ""):
        name = "Hymne"
    elif not name and "psaume_" in elt.get("element_key", ""):
        name = "Psaume"
    elif not name and "antienne_" in elt.get("element_key", ""):
        name = "Antienne"
    elif not name and "repons" in elt.get("element_key", ""):
        name = "Répons"
    elif not name and "pericope" in elt.get("element_key", ""):
        name = "Parole de Dieu"
    elif not name and "intercession" in elt.get("element_key", ""):
        name = "Intercession"
    elif not name and "benediction" in elt.get("element_key", ""):
        name = "Bénédiction"
    elif not name and "oraison" in elt.get("element_key", ""):
        name = "Oraison"
    #

    #
    reference = elt.get("reference", "")

    if reference:
        reference = parseName(reference)
        write_message("Problème pour les références de psaume du style '30 - I'",level="debug")
    #

    if name.lower() in reference.lower():
        name=None
    #

    if reference and name:
        name = f"{name}, *{reference}*"
    elif reference:
        name = f"*{reference}*"
    #

    author = elt.get("auteur", "")
    editor = elt.get("editeur", "")
    if author and editor:
        post = f"{author}, {editor}"
    elif author:
        post = author
    elif editor:
        post = editor
    else:
        post = "" # a conserver !!!
    #

    if post:
        name = f"{name} ({post})"
    #

    return name if name else "_ _"
