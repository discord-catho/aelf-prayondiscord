from dktotoolkit import write_message

from ._item_to_fieldname import _item_to_fieldname

def _content_to_fields(datas, verbose=None):
    """
    :param list datas:
    """

    out = []
    if not isinstance(datas, list):
        write_message(f"ERROR {type(datas)} :: {datas}", level="critical")

        raise ValueError
    #

    inline = False

    for elt in datas:

        name = _item_to_fieldname(elt)
        text = elt.get("texte", "")
        same_page = bool(elt.get("same_page", False))
        force_same_page = bool(elt.get("force_same_page", False))

        if not (name and text):
            write_message(f"Neither name nor text here: {elt}", level='debug')
            continue

        if not text:
            write_message(f"Not text here {elt}", level='debug')
            continue
        #
        #raise Exception("Il faut corriger cette fonction, qui découpe les elements mais ne renvoie que le premier les autres etant vides")
        if isinstance(text, list) and len(text)==1:
            text=text[0]
        #

        if isinstance(text, str):

            value=text.strip("\n")

            if len(value) > 1024:
                write_message(f"Value too long : {len(value)} :: {str(value)}", level="critical")
                raise ValueError(f"Value too long : {len(value)} > 1024 !")
            #

            td = {"name":name, "value":value, "inline": inline, "same_page": False, "force_same_page": same_page or force_same_page}
            out += [td,]

        elif isinstance(text, list):
            len_text = len(text)

            for i in range(len_text):
                td = {
                    "name": "" if i > 0 else name,
                    "value": "_ _\n" + text[i] if i > 0 else text[i],
                    "inline": False,
                    "same_page": i > 0,
                    "force_same_page": (same_page or force_same_page) if i == 0 else False,
                }
                out.append(td)
            #
        else:
            write_message(f"Type of text: {type(text)} :: {str(text)}", level="critical")
            raise ValueError(f"Type of text: {type(text)}")
        #

    #
    return out
#
