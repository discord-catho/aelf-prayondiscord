from .tools.colorname2code import colorname2code
from .tools.insert_doxo import insert_doxologie

all = [
    'colorname2code',
    'insert_doxologie'
]
