# -*- coding: utf-8 -*-
"""
Créé le Vendredi 7 Octobre 2022 à 19:14:22
@author: Julien
"""

correspondances = {
    "Gn": "livre de la Genèse",
    "Ex": "Livre de l'Exode",
    "Lv": "Livre du Lévitique",
    "Nb": "Livre des Nombres",
    "Dt": "Livre du Deutéronome",
    "Jos": "Livre de Josué",
    "Jg": "Livre des Juges",
    "Rt": "Livre de Ruth",
    "1 S": "Premier livre de Samuel",
    "2 S": "Deuxième livre de Samuel",
    "1 R": "Premier livre des Rois",
    "2 R": "Deuxième livre des Rois",
    "1 Ch": "Premier livre des Chroniques",
    "2 Ch": "Deuxième livre des Chroniques",
    "Esd": "Livre d'Esdras",
    "Ne": "Livre de Néhémie",
    "Tb": "Livre de Tobie",
    "Jdt": "Livre de Judith",
    "Est": "Livre d'Esther",
    "1 M": "Premier Livre des Martyrs d'Israël",
    "2 M": "Deuxième Livre des Martyrs d'Israël",
    "Jb": "Livre de Job",
    "Pr": "Livre des Proverbes",
    "Qo": "L'ecclésiaste",
    "Ct": "Cantique des cantiques",
    "Sg": "Livre de la Sagesse",
    "Si": "Livre de Ben Sira le Sage",
    "Is": "Livre d'Isaïe",
    "Jr": "Livre de Jérémie",
    "Lm": "Livre des lamentations de Jérémie",
    "Ba": "Livre de Baruch",
    "Ez": "Livre d'Ezekiel",
    "Dn": "Livre de Daniel",
    "Os": "Livre d'Osée",
    "Jl": "Livre de Joël",
    "Am": "Livre d'Amos",
    "Ab": "Livre d'Abdias",
    "Jon": "Livre de Jonas",
    "Mi": "Livre de Michée",
    "Na": "Livre de Nahum",
    "Ha": "Livre d'Habaquc",
    "So": "Livre de Sophonie",
    "Ag": "Livre d'Aggée",
    "Za": "Livre de Zacharie",
    "Ml": "Livre de Malachie",
    "Mt": "Évangile de Jésus-Christ selon saint Matthieu",
    "Mc": "Évangile de Jésus-Christ selon saint Marc",
    "Lc": "Évangile de Jésus-Christ selon saint Luc",
    "Jn": "Évangile de Jésus-Christ selon saint Jean",
    "Ac": "Livre des Actes des Apôtres",
    "Rm": "Lettre de saint Paul Apôtre aux Romains",
    "1 Co": "Première lettre de saint Paul Apôtre aux Corinthiens",
    "2 Co": "Deuxième lettre de saint Paul Apôtre aux Corinthiens",
    "Ga": "Lettre de saint Paul Apôtre aux Galates",
    "Ep": "Lettre de saint Paul Apôtre aux Ephésiens",
    "Ph": "Lettre de saint Paul Apôtre aux Philippiens",
    "Col": "Lettre de saint Paul Apôtre aux Colossiens",
    "1 Th": "Première lettre de saint Paul Apôtre aux Thessaloniciens",
    "2 Th": "Deuxième lettre de saint Paul Apôtre aux Thessaloniciens",
    "1 Tm": "Première lettre de saint Paul Apôtre à Timothée",
    "2 Tm": "Deuxième lettre de saint Paul Apôtre à Timothée",
    "Tt": "Lettre de saint Paul Apôtre à Tite",
    "Phm": "Lettre de saint Paul Apôtre à Philémon",
    "He": "Lettre aux Hébreux",
    "Jc": "Lettre de saint Jacques Apôtre",
    "1 P": "Première lettre de saint Pierre Apôtre",
    "2 P": "Deuxième lettre de saint Pierre Apôtre",
    "1 Jn": "Première lettre de saint Jean",
    "2 Jn": "Deuxième lettre de saint Jean",
    "3 Jn": "Troisième lettre de saint Jean",
    "Jude": "Lettre de saint Jude",
    "Ap": "Livre de l'Apocalypse",
    "Ps": "Psaume"
}

def parseName(name):

    if isinstance(name, int):
        return f"Psaume {name}"
    elif name.isdigit():
        return f"Psaume {name}"
    #endIf

    if len(name)>4 and "cf." in name[0:4]:
        name = name[4:]
    #endIf

    data = [e.strip() for e in name.split(",")]

    if len(data) > 1:
        verset = (data[1].split("-"))
    else:
        verset = []
    #endIf

    datasplit = data[0].split(" ")

    if len(datasplit) == 1:
        return " ".join(datasplit)
    elif len(datasplit) == 2:
        livreAbrev = datasplit[0]
        chapitre = datasplit[1]
    else:
        livreAbrev = datasplit[0] + " " + datasplit[1]
        chapitre = datasplit[2]
    #endIf

    if not correspondances.get(livreAbrev):
        return name.capitalize()
    #

    if len(verset) == 2 :
        return f"{correspondances[livreAbrev]}, chapitre {chapitre}, versets {verset[0]} à {verset[1]}"
    elif len(verset) == 1 :
        return f"{correspondances[livreAbrev]}, chapitre {chapitre}, verset {verset[0]}"
    elif chapitre:
        return f"{correspondances[livreAbrev]}, chapitre {chapitre}"
    else:
        return f"{correspondances[livreAbrev]}"
    #endIf

#endDef
