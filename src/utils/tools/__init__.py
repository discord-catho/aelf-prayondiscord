from .colorname2code import colorname2code
from .insert_doxo import insert_doxologie
from .parsing_book_name import parseName

all=[
    "colorname2code",
    "insert_doxologie",
    "parseName"
]

