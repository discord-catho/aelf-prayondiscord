from dktotoolkit import write_message
couleurs_liturgiques ={
    "blanc":0xffffff,
    "noir":0x000000,
    "violet":0x93188d,
    "rouge":0xbe0000,
    "rose":0xff76b9,
    "vert":0x149231
}

def colorname2code(color):

    if isinstance(color, str):

        if color.lower() in couleurs_liturgiques.keys():
            return couleurs_liturgiques[color.lower()]
        else:
            write_message(f"ERREUR (1) : color not found !", level="warning")
            return 0x63b1ff # bleu ciel

        #endIf

    elif isinstance(color, int):
        return color
    else:
        write_message(f"ERREUR (2) : TYPEERROR !", level="critical")
        #return 0xffd263 # orange
        raise TypeError
    #endIf
#endDef
