import os
import sys
import logging
import asyncio
from dktotoolkit import task_watch_dotenv_file, load_dotenv

from lib.logging import CustomLogger
from core import Bot

# A modifier si necessaire
PREFIX_BOT="BOTAELF"
SLEEP_TIME=2
# Fin a modifier


async def main(verbose:bool=False):
    sys.stdout.write(f"> Start bot: {__file__}\n")
    sys.stdout.write("-"*45+"\n")

    # Set logger
    logging.getLogger()
    CustomLogger().setup_logger(reset_logs=True, logs_directory="./logs")

    # Démarrer la tâche asynchrone de surveillance du fichier .env
    asyncio.create_task(task_watch_dotenv_file(filename="./.env", verbose=verbose))
    await asyncio.sleep(2)  # Attendre que le fichier dotenv soit chargé

    # Démarrer le bot
    async with Bot(prefix_environ=PREFIX_BOT, verbose=verbose) as bot:

        await asyncio.sleep(1)
        await bot.run()
    #

#

if __name__=="__main__":
    verbose = True
    asyncio.run(main(verbose=verbose))
#endIf

